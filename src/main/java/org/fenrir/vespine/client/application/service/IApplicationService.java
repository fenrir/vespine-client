package org.fenrir.vespine.client.application.service;

import org.fenrir.vespine.spi.exception.BusinessException;

public interface IApplicationService 
{
	public void checkRestAPICompatibility(String version) throws BusinessException;
}
