package org.fenrir.vespine.client.application.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.application.service.IApplicationService;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140807
 */
public class ApplicationServiceImpl implements IApplicationService 
{
	private WebResource resource;
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resource = Client.create().resource(endpoint);
	}
	
	@Override
	public void checkRestAPICompatibility(String version) throws BusinessException 
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}

		ClientResponse response = resource
				.path(RESTConstants.URL_API)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.get(ClientResponse.class);
		
		String faultCause = null;
		String strResponse = null;
		// Error
		if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			faultCause = response.getEntity(String.class);
		}
		else{
			strResponse = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error intern del servidor: " + faultCause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			throw new BusinessException("Error sense especificar: " + faultCause);	
		}
		
		try{
			JSONObject apiData = new JSONObject(strResponse);
			String minVersion = apiData.getString("minVersion");
			String currentVersion = apiData.getString("currentVersion");
			
			if(version.compareTo(minVersion)<0){
				throw new BusinessException("La versió de l'aplicació és incompatible amb el servidor especificat [" + version + "]; Versió mínima requerida [" + minVersion + "]");
			}
			if(version.compareTo(currentVersion)>0){
				throw new BusinessException("La versió de l'aplicació és incompatible amb el servidor especificat [" + version + "]; Versió máxima permesa [" + currentVersion + "]");
			}
		}
		catch(JSONException e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
}
