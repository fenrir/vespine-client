package org.fenrir.vespine.client.authentication.builder;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.authentication.dto.AuthenticatedUserJsonDTO;
import org.fenrir.vespine.client.authentication.dto.AuthenticationPermissionJsonDTO;
import org.fenrir.vespine.client.authentication.dto.AuthenticationRoleJsonDTO;

public class AuthenticatedUserJsonBuilder 
{
	private JSONObject jsonObj;
	
	public AuthenticatedUserJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public AuthenticatedUserJsonDTO build() throws JSONException
	{
		String username = jsonObj.getString("username");
		JSONArray roleList = jsonObj.getJSONArray("roles");
		JSONArray permissionList = jsonObj.getJSONArray("permissions");
		
		// Dades bàsiques
		AuthenticatedUserJsonDTO dto = new AuthenticatedUserJsonDTO();
		dto.setUsername(username);
		// Dades rols
		List<AuthenticationRoleJsonDTO> roleDTOList = new ArrayList<AuthenticationRoleJsonDTO>();
		for(int i=0; i<roleList.length(); i++){
			JSONObject role = roleList.getJSONObject(i);
			String name = role.getString("name");
			String description = role.getString("description");
			AuthenticationRoleJsonDTO roleDTO = new AuthenticationRoleJsonDTO();
			roleDTO.setName(name);
			roleDTO.setDescription(description);
			
			roleDTOList.add(roleDTO);
		}
		dto.setRoles(roleDTOList);
		// Dades permisos
		List<AuthenticationPermissionJsonDTO> permissionDTOList = new ArrayList<AuthenticationPermissionJsonDTO>();
		for(int i=0; i<permissionList.length(); i++){
			JSONObject permissions = permissionList.getJSONObject(i);
			String name = permissions.getString("value");
			String description = permissions.getString("description");
			AuthenticationPermissionJsonDTO permission = new AuthenticationPermissionJsonDTO();
			permission.setValue(name);
			permission.setDescription(description);
			
			permissionDTOList.add(permission);
		}
		dto.setPermissions(permissionDTOList);
		
		return dto;
	}
}
