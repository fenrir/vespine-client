package org.fenrir.vespine.client.authentication.dto;

import java.util.Collections;
import java.util.List;

public class AuthenticatedUserJsonDTO 
{
	private String username;
	private List<AuthenticationRoleJsonDTO> roles;
	private List<AuthenticationPermissionJsonDTO> permissions;

	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public List<AuthenticationRoleJsonDTO> getRoles()
	{
		if(roles==null){
			return Collections.emptyList();
		}
		return roles;
	}
	
	public void setRoles(List<AuthenticationRoleJsonDTO> roles)
	{
		this.roles = roles;
	}
	
	public List<AuthenticationPermissionJsonDTO> getPermissions()
	{
		if(permissions==null){
			return Collections.emptyList();
		}
		return permissions;
	}
	
	public void setPermissions(List<AuthenticationPermissionJsonDTO> permissions)
	{
		this.permissions = permissions;
	}
	
	@Override
	public String toString()
	{
		return username;
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof AuthenticatedUserJsonDTO)){
            return false;
        }
        AuthenticatedUserJsonDTO other = (AuthenticatedUserJsonDTO) obj;
        if(getUsername()==null){
            if(other.getUsername()!=null){
                return false;
            }
        }
        else if(!getUsername().equals(other.getUsername())){
            return false;
        }

        return true;
    }
}
