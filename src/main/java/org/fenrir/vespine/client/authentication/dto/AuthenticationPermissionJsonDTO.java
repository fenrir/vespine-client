package org.fenrir.vespine.client.authentication.dto;

public class AuthenticationPermissionJsonDTO 
{
	private String value;
	private String description;
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof AuthenticationPermissionJsonDTO)){
            return false;
        }
        AuthenticationPermissionJsonDTO other = (AuthenticationPermissionJsonDTO) obj;
        if(getValue()==null){
            if(other.getValue()!=null){
                return false;
            }
        }
        else if(!getValue().equals(other.getValue())){
            return false;
        }

        return true;
    }
}
