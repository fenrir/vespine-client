package org.fenrir.vespine.client.authentication.dto;

public class AuthenticationRoleJsonDTO 
{
	private String name;
	private String description;
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof AuthenticationRoleJsonDTO)){
            return false;
        }
        AuthenticationRoleJsonDTO other = (AuthenticationRoleJsonDTO) obj;
        if(getName()==null){
            if(other.getName()!=null){
                return false;
            }
        }
        else if(!getName().equals(other.getName())){
            return false;
        }

        return true;
    }
}
