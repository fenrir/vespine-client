package org.fenrir.vespine.client.authentication.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140724
 */
@SuppressWarnings("serial")
public class VespineAuthenticationException extends Exception
{
	public VespineAuthenticationException()
    {
        super();
    }

    public VespineAuthenticationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public VespineAuthenticationException(String message)
    {
        super(message);
    }

    public VespineAuthenticationException(Throwable cause)
    {
        super(cause);
    }
}
