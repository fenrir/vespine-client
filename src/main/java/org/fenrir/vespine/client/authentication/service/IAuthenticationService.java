package org.fenrir.vespine.client.authentication.service;

import org.fenrir.vespine.client.authentication.dto.AuthenticatedUserJsonDTO;
import org.fenrir.vespine.client.authentication.exception.VespineAuthenticationException;
import org.fenrir.vespine.spi.exception.BusinessException;

public interface IAuthenticationService 
{
	public AuthenticatedUserJsonDTO login(String user, String password) throws VespineAuthenticationException, BusinessException;
}
