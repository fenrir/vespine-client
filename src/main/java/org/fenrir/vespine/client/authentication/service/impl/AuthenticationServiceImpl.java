package org.fenrir.vespine.client.authentication.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.authentication.builder.AuthenticatedUserJsonBuilder;
import org.fenrir.vespine.client.authentication.dto.AuthenticatedUserJsonDTO;
import org.fenrir.vespine.client.authentication.exception.VespineAuthenticationException;
import org.fenrir.vespine.client.authentication.service.IAuthenticationService;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140729
 */
public class AuthenticationServiceImpl implements IAuthenticationService 
{
	private WebResource resource;
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resource = Client.create().resource(endpoint);
	}
	
	@Override
	public AuthenticatedUserJsonDTO login(String user, String password) throws VespineAuthenticationException, BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}

		ClientResponse response = resource.path(RESTConstants.URL_APPLICATION_LOGIN)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encode(user + ":" + password)))
				.post(ClientResponse.class);
		
		String faultCause = null;
		String strResponse = null;
		// Error
		if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			faultCause = response.getEntity(String.class);
		}
		else{
			strResponse = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()){
			throw new VespineAuthenticationException("Login incorrecte: " + faultCause);
		}
		else if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error intern del servidor: " + faultCause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			throw new BusinessException("Error sense especificar: " + faultCause);	
		}
		
		try{
			JSONObject userData = new JSONObject(strResponse);
			AuthenticatedUserJsonBuilder builder = new AuthenticatedUserJsonBuilder();
			builder.setJsonObj(userData);
			return builder.build();
		}
		catch(JSONException e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
}
