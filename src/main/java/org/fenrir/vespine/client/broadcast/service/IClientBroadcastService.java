package org.fenrir.vespine.client.broadcast.service;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
public interface IClientBroadcastService 
{
	public String getConnectionId();
	public void connect() throws Exception;
	public void disconnect();
}
