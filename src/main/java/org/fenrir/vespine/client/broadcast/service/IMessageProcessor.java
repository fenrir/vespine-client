package org.fenrir.vespine.client.broadcast.service;

import org.fenrir.vespine.core.dto.BroadcastMessage;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public interface IMessageProcessor 
{
	public void processMessage(BroadcastMessage message);
}
