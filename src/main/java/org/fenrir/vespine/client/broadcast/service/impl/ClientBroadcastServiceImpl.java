package org.fenrir.vespine.client.broadcast.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import org.atmosphere.wasync.Client;
import org.atmosphere.wasync.ClientFactory;
import org.atmosphere.wasync.Decoder;
import org.atmosphere.wasync.Event;
import org.atmosphere.wasync.Function;
import org.atmosphere.wasync.Request;
import org.atmosphere.wasync.RequestBuilder;
import org.atmosphere.wasync.Socket;
import org.atmosphere.wasync.impl.AtmosphereClient;
import org.codehaus.jackson.map.ObjectMapper;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.providers.grizzly.GrizzlyAsyncHttpProvider;
import com.sun.jersey.core.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.broadcast.service.IMessageProcessor;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.vespine.core.dto.BroadcastMessage;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140815
 */
public class ClientBroadcastServiceImpl implements IClientBroadcastService 
{
	private final Logger log = LoggerFactory.getLogger(ClientBroadcastServiceImpl.class);

	private String connectionId;
	private String endpoint;
	private Client client;
	private Socket socket;
	
	@Inject
	private IUserCredentialsProvider credentialsProvider;
	
	@Inject
	private IMessageProcessor messageProcessor;
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	public void setMessageProcessor(IMessageProcessor messageProcessor)
	{
		this.messageProcessor = messageProcessor;
	}
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		this.endpoint = endpoint;
		client = ClientFactory.getDefault().newClient(AtmosphereClient.class);
	}
	
	@Override
	public String getConnectionId()
	{
		return connectionId;
	}
	
	@Override
	public void connect() throws Exception
	{
		if(socket!=null){
			return;
		}
		
		RequestBuilder requestBuilder = client.newRequestBuilder()
                .method(Request.METHOD.POST)
                .header(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encode(credentialsProvider.getUsername() + ":" + credentialsProvider.getPassword())))
                .uri(endpoint + RESTConstants.URL_BROADCAST_SUBSCRIBE)
                .decoder(new Decoder<String, BroadcastMessage>() 
        		{
                    @Override
                    public BroadcastMessage decode(Event type, String data) 
                    {
                        final String message = data.trim();
                        // Padding Atmosphere, ignorar
                        if(message.length()==0){
                            return null;
                        }

                        if(type.equals(Event.MESSAGE)){
                        	ObjectMapper mapper = new ObjectMapper();
                        	try{
                        		return mapper.readValue(message, BroadcastMessage.class);
                        	}
                        	catch(Exception e)
                        	{
                        		log.error("Error interpretant missatge: {}", e.getMessage(), e);
                        		log.error("Dades rebudes: {}", message);
                        		throw new RuntimeException("Error interpretant missatge: " + e.getMessage(), e);
                        	}
                        } 
                        else{
                            return null;
                        }
                    }
                })
//                .transport(Request.TRANSPORT.WEBSOCKET)
                .transport(Request.TRANSPORT.STREAMING)
                .transport(Request.TRANSPORT.LONG_POLLING);
		
		AsyncHttpClient clientRuntime = new AsyncHttpClient(new GrizzlyAsyncHttpProvider(new AsyncHttpClientConfig.Builder().build()));
		socket = client.create(client.newOptionsBuilder().runtime(clientRuntime).build())
				.on(Event.MESSAGE, new Function<BroadcastMessage>() 
				{
					@Override
					public void on(BroadcastMessage message) 
					{
						if(BroadcastMessage.MESSAGE_TYPE_CONNECTION.equals(message.getType())){
							connectionId = (String)message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_CONNECTION_ID);
							log.info("Connexió establerta. ID={}", connectionId);
						}
						else{
							log.info("Rebut missatge broadcast '{}' amb origen {}", message.toString(), message.getOriginator());
							// Només es processa el missatge si l'originador != a l'aplicació
							if(connectionId==null || !connectionId.equals(message.getOriginator())){
								messageProcessor.processMessage(message);
							}
						}
					}
				});
		socket.open(requestBuilder.build());
	}
	
	@Override
	public void disconnect()
	{
		socket.close();
		socket = null;
	}
}
