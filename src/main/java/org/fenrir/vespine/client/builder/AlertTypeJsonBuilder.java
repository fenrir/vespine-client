package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.AlertTypeJsonDTO;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140125
 */
public class AlertTypeJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(AlertTypeJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public AlertTypeJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IAlertTypeDTO build() throws JSONException
	{
		String alertTypeId = jsonObj.getString("alertTypeId");
		String name = jsonObj.getString("name");
		String description = jsonObj.getString("description");
		String icon = jsonObj.getString("icon");
		String alertRule = jsonObj.getString("alertRule");
		String creationDate = jsonObj.getString("creationDate");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		AlertTypeJsonDTO dto = new AlertTypeJsonDTO();
		dto.setAlertTypeId(alertTypeId);
		dto.setName(name);
		dto.setDescription(description);
		dto.setAlertRule(alertRule);
		dto.setIcon(icon);
		try{
			dto.setCreationDate(new SimpleDateFormat("yyyy-MM-dd").parse(creationDate));
		}
		catch(ParseException e){
			log.error("Error interpretant data de creació del tipus d'alerta {}: {}", new Object[]{alertTypeId, e.getMessage(), e});
		}
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
