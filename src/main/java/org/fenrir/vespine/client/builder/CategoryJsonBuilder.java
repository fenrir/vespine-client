package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.CategoryJsonDTO;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class CategoryJsonBuilder 
{
	private JSONObject jsonObj;
	
	public CategoryJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ICategoryDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String provider = jsonObj.getString("provider");
		String category = jsonObj.getString("categoryId");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONArray providerCategoryList = jsonObj.getJSONArray("providerCategories");
		
		// Dades bàsiques
		CategoryJsonDTO dto = new CategoryJsonDTO();
		dto.setName(name);
		dto.setProvider(provider);
		dto.setCategoryId(category);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades provider elements
		List<IProviderElementDTO> providerElemDTOList = new ArrayList<IProviderElementDTO>();
		for(int i=0; i<providerCategoryList.length(); i++){
			JSONObject elem = providerCategoryList.getJSONObject(i);
			ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
			builder.setJsonObj(elem);
			providerElemDTOList.add(builder.build());
		}
		dto.setProviderCategories(providerElemDTOList);
		
		return dto;
	}
}
