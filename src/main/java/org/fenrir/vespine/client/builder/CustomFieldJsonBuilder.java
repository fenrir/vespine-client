package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.CustomFieldJsonDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IProjectDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class CustomFieldJsonBuilder 
{
	private JSONObject jsonObj;
	
	public CustomFieldJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ICustomFieldDTO build() throws JSONException, ClassNotFoundException
	{
		String fieldId = jsonObj.getString("fieldId");
		String name = jsonObj.getString("name");
		boolean mandatory = jsonObj.getBoolean("mandatory");
		String provider = jsonObj.getString("provider");
		String dataProviderId = jsonObj.getString("dataProviderId");
		JSONObject jsonFieldType = jsonObj.getJSONObject("fieldType");
		FieldTypeJsonBuilder typeBuilder = new FieldTypeJsonBuilder();
		IFieldType fieldType = typeBuilder.setJsonObj(jsonFieldType).build();
		JSONArray projectList = jsonObj.getJSONArray("projects");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		CustomFieldJsonDTO dto = new CustomFieldJsonDTO();
		dto.setFieldId(fieldId);
		dto.setName(name);
		dto.setMandatory(mandatory);
		dto.setProvider(provider);
		dto.setDataProviderId(dataProviderId);
		dto.setFieldType(fieldType);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades projectes associats
		List<IProjectDTO> projectDTOList = new ArrayList<IProjectDTO>();
		for(int i=0; i<projectList.length(); i++){
			JSONObject elem = projectList.getJSONObject(i);
			ProjectJsonBuilder projectBuilder = new ProjectJsonBuilder();
			projectBuilder.setJsonObj(elem);
			projectDTOList.add(projectBuilder.build());
		}
		dto.setProjects(projectDTOList);
		
		return dto;
	}
}
