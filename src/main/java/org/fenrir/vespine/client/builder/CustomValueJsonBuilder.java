package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.CustomValueJsonDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140717
 */
public class CustomValueJsonBuilder 
{
	private JSONObject jsonObj;
	
	public CustomValueJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ICustomValueDTO build() throws JSONException, ClassNotFoundException
	{
		String valueId = jsonObj.getString("valueId");
		String provider = jsonObj.getString("provider");
		JSONObject jsonField = jsonObj.getJSONObject("field");
		CustomFieldJsonBuilder fieldBuilder = new CustomFieldJsonBuilder();
		ICustomFieldDTO field = fieldBuilder.setJsonObj(jsonField).build();
		JSONObject jsonIssue = jsonObj.getJSONObject("issue");
		IssueJsonBuilder issueBuilder = new IssueJsonBuilder();
		IIssueDTO issue = issueBuilder.setJsonObj(jsonIssue).build();
		String value = jsonObj.getString("value");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		CustomValueJsonDTO dto = new CustomValueJsonDTO();
		dto.setValueId(valueId);
		dto.setProvider(provider);
		dto.setField(field);
		dto.setIssue(issue);
		dto.setValue(value);
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
