package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.DictionaryItemJsonDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryItemJsonBuilder 
{
	private JSONObject jsonObj;
	
	public DictionaryItemJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IDictionaryItemDTO build() throws JSONException
	{
		String itemId = jsonObj.getString("itemId");
		String description = jsonObj.getString("description");
		String value = jsonObj.getString("value");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		DictionaryItemJsonDTO dto = new DictionaryItemJsonDTO();
		dto.setItemId(itemId);
		dto.setDescription(description);
		dto.setValue(value);
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
