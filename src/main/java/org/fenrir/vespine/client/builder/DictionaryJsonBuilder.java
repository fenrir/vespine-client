package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.DictionaryJsonDTO;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryJsonBuilder 
{
	private JSONObject jsonObj;
	
	public DictionaryJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IDictionaryDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String dictionaryId = jsonObj.getString("dictionaryId");
		String description = jsonObj.getString("description");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONArray dictionaryItemList = jsonObj.getJSONArray("dictionaryItems");
		
		// Dades bàsiques
		DictionaryJsonDTO dto = new DictionaryJsonDTO();
		dto.setDictionaryId(dictionaryId);
		dto.setName(name);
		dto.setDescription(description);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades items
		List<IDictionaryItemDTO> itemDTOList = new ArrayList<IDictionaryItemDTO>();
		for(int i=0; i<dictionaryItemList.length(); i++){
			JSONObject step = dictionaryItemList.getJSONObject(i);
			DictionaryItemJsonBuilder builder = new DictionaryItemJsonBuilder();
			builder.setJsonObj(step);
			itemDTOList.add(builder.build());
		}
		dto.setDictionaryItems(itemDTOList);
		
		return dto;
	}
}
