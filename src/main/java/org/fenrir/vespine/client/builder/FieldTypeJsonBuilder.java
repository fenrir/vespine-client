package org.fenrir.vespine.client.builder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.FieldTypeJsonDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class FieldTypeJsonBuilder 
{
	private JSONObject jsonObj;
	
	public FieldTypeJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IFieldType build() throws JSONException, ClassNotFoundException
	{
		String type = jsonObj.getString("type");
		String strConverter = jsonObj.getString("converter");
		Class<?> converter = Class.forName(strConverter);
		boolean dataProviderId = jsonObj.getBoolean("dataProviderid");
		
		Map<String, String> converterParameters = new HashMap<String, String>();
		JSONObject parametersMap = jsonObj.getJSONObject("converterParameters");
		if(parametersMap!=null){
			Iterator<String> iterator = parametersMap.keys();
			while(iterator.hasNext()){
				String key = iterator.next();
				String value = parametersMap.getString(key);
				parametersMap.put(key, value);
			}
		}
		
		FieldTypeJsonDTO dto = new FieldTypeJsonDTO();
		dto.setType(type);
		dto.setConverter((Class<? extends IAttributeConverter<?, ?>>)converter);
		dto.setDataProviderId(dataProviderId);
		dto.setConverterParameters(converterParameters);
		
		return dto;
	}
}
