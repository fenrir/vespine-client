package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.IssueAlertJsonDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueAlertJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(IssueAlertJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public IssueAlertJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IIssueAlertDTO build() throws JSONException
	{
		String alertId = jsonObj.getString("alertId");
		String creationDate = jsonObj.getString("creationDate");
		JSONObject jsonIssue = jsonObj.getJSONObject("issue");
		JSONObject jsonType = jsonObj.getJSONObject("type");
		
		IssueAlertJsonDTO dto = new IssueAlertJsonDTO();
		dto.setAlertId(alertId);
		try{
			dto.setCreationDate(new SimpleDateFormat("yyyy-MM-dd").parse(creationDate));
		}
		catch(ParseException e){
			log.error("Error interpretant d'inici / final de l'alerta {}: {}", new Object[]{alertId, e.getMessage(), e});
		}
		IssueJsonBuilder issueBuilder = new IssueJsonBuilder();
		dto.setIssue(issueBuilder.setJsonObj(jsonIssue).build());
		AlertTypeJsonBuilder typeBuilder = new AlertTypeJsonBuilder();
		dto.setType(typeBuilder.setJsonObj(jsonType).build());
		
		return dto;
	}
}
