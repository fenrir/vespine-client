package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.IssueJsonDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(IssueJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public IssueJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IIssueDTO build() throws JSONException
	{
		String provider = jsonObj.getString("provider");
		String issueId = jsonObj.getString("issueId");
		String visibleId = jsonObj.getString("visibleId");
		String summary = jsonObj.getString("summary");
		String description = jsonObj.getString("description");
		JSONObject jsonStatus = jsonObj.getJSONObject("status");
		JSONObject jsonProject = jsonObj.getJSONObject("project");
		JSONObject jsonSeverity = jsonObj.getJSONObject("severity");
		JSONObject jsonCategory = jsonObj.getJSONObject("category");
		JSONObject jsonReporter = jsonObj.getJSONObject("reporter");
		JSONObject jsonAssignedUser = jsonObj.optJSONObject("assignedUser");
		Long sendDate = jsonObj.getLong("sendDate");
		Long resolutionDate = !jsonObj.isNull("resolutionDate") ? jsonObj.getLong("resolutionDate") : null;
		Long modifiedDate = jsonObj.getLong("modifiedDate");
		String sla = jsonObj.optString("sla");
	    String slaDate = !jsonObj.isNull("slaDate") ? jsonObj.getString("slaDate") : null;
	    Integer estimatedTime = !jsonObj.isNull("estimatedTime") ? jsonObj.getInt("estimatedTime") : null;
	    String estimatedTimeFormated = !jsonObj.isNull("estimatedTimeFormated") ? jsonObj.getString("estimatedTimeFormated") : null;
	    JSONArray tagList = jsonObj.getJSONArray("tags");
	    JSONArray noteList = jsonObj.getJSONArray("notes");
	    Long lastUpdated = jsonObj.getLong("lastUpdated");
	    JSONObject jsonSprint = jsonObj.optJSONObject("sprint");
	    JSONArray workRegistryList = jsonObj.getJSONArray("workRegistries");
	    Integer workTime = jsonObj.getInt("workTime");
	    String workTimeFormated = jsonObj.getString("workTimeFormated");
	    String deleted = jsonObj.getString("deleted");
	    
	    IssueJsonDTO dto = new IssueJsonDTO();
	    dto.setProvider(provider);
	    dto.setIssueId(issueId);
	    dto.setVisibleId(visibleId);
	    dto.setSummary(summary);
	    dto.setSummary(summary);
	    dto.setDescription(description);

	    StatusJsonBuilder statusBuilder = new StatusJsonBuilder();
	    dto.setStatus(statusBuilder.setJsonObj(jsonStatus).build());
	    ProjectJsonBuilder projectBuilder = new ProjectJsonBuilder();
	    dto.setProject(projectBuilder.setJsonObj(jsonProject).build());
	    SeverityJsonBuilder severityBuilder = new SeverityJsonBuilder();
	    dto.setSeverity(severityBuilder.setJsonObj(jsonSeverity).build());
	    CategoryJsonBuilder categoryBuilder = new CategoryJsonBuilder();
	    dto.setCategory(categoryBuilder.setJsonObj(jsonCategory).build());
	    
	    UserJsonBuilder userBuilder = new UserJsonBuilder();
    	dto.setReporter(userBuilder.setJsonObj(jsonReporter).build());
	    if(jsonAssignedUser!=null){
	    	dto.setAssignedUser(userBuilder.setJsonObj(jsonAssignedUser).build());
	    }
	    
	    dto.setSendDate(new Date(sendDate));
	    if(resolutionDate!=null){
	    	dto.setResolutionDate(new Date(resolutionDate));
	    }
	    dto.setModifiedDate(new Date(modifiedDate));
	    dto.setSla(Boolean.valueOf(sla));
	    if(slaDate!=null){
		    try{
				dto.setSlaDate(new SimpleDateFormat("yyyy-MM-dd").parse(slaDate));
			}
			catch(ParseException e){
				log.error("Error interpretant la data SLA de l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
			}
	    }
	    dto.setEstimatedTime(estimatedTime);
	    dto.setEstimatedTimeFormated(estimatedTimeFormated);
	    dto.setLastUpdated(new Date(lastUpdated));

	    if(jsonSprint!=null){
	    	SprintJsonBuilder sprintBuilder = new SprintJsonBuilder();
	    	dto.setSprint(sprintBuilder.setJsonObj(jsonSprint).build());
	    }
	    
	    dto.setWorkTime(workTime);
	    dto.setWorkTimeFormated(workTimeFormated);
	    dto.setDeleted(Boolean.valueOf(deleted));
	    
		List<ITagDTO> tagDTOList = new ArrayList<ITagDTO>();
		for(int i=0; i<tagList.length(); i++){
			JSONObject tag = tagList.getJSONObject(i);
			TagJsonBuilder tagBuilder = new TagJsonBuilder();
			tagDTOList.add(tagBuilder.setJsonObj(tag).build());
		}
		dto.setTags(tagDTOList);
		
		List<IIssueNoteDTO> noteDTOList = new ArrayList<IIssueNoteDTO>();
		for(int i=0; i<noteList.length(); i++){
			JSONObject note = noteList.getJSONObject(i);
			IssueNoteJsonBuilder noteBuilder = new IssueNoteJsonBuilder();
			noteDTOList.add(noteBuilder.setJsonObj(note).build());
		}
		dto.setNotes(noteDTOList);
		
		List<IWorkRegistryDTO> workRegistryDTOList = new ArrayList<IWorkRegistryDTO>();
		for(int i=0; i<workRegistryList.length(); i++){
			JSONObject registry = workRegistryList.getJSONObject(i);
			WorkRegistryJsonBuilder  workRegistryBuilder = new WorkRegistryJsonBuilder();
			workRegistryDTOList.add(workRegistryBuilder.setJsonObj(registry).build());
		}
		dto.setWorkRegistries(workRegistryDTOList);
	    
	    return dto;
	}
}
