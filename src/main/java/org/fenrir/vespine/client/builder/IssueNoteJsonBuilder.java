package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.IssueNoteJsonDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueNoteJsonBuilder 
{
	private JSONObject jsonObj;
	
	public IssueNoteJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IIssueNoteDTO build() throws JSONException
	{
		String noteId = jsonObj.getString("noteId");
		String provider = jsonObj.getString("provider");
		String reporter = jsonObj.getString("reporter");
		String text = jsonObj.getString("text");
		Long sendDate = jsonObj.getLong("sendDate");
		Long lastEditionDate = jsonObj.getLong("lastEditionDate");
		
		IssueNoteJsonDTO dto = new IssueNoteJsonDTO();
		dto.setNoteId(noteId);
		dto.setProvider(provider);
		// Json backrefenrence
		dto.setIssue(null);
		dto.setReporter(reporter);
		dto.setText(text);
		dto.setSendDate(new Date(sendDate));
		dto.setLastEditionDate(new Date(lastEditionDate));
		
		return dto;
	}
}
