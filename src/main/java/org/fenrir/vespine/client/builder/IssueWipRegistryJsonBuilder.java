package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.IssueWipRegistryJsonDTO;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueWipRegistryJsonBuilder 
{
	private JSONObject jsonObj;
	
	public IssueWipRegistryJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IIssueWipRegistryDTO build() throws JSONException
	{
		String registryId = jsonObj.getString("registryId");
		JSONObject jsonIssue = jsonObj.getJSONObject("issue");
		JSONObject jsonUser = jsonObj.getJSONObject("user");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		IssueWipRegistryJsonDTO dto = new IssueWipRegistryJsonDTO();
		dto.setRegistryId(registryId);
		IssueJsonBuilder issueBuilder = new IssueJsonBuilder();
		dto.setIssue(issueBuilder.setJsonObj(jsonIssue).build());
		UserJsonBuilder userBuilder = new UserJsonBuilder();
		dto.setUser(userBuilder.setJsonObj(jsonUser).build());
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
