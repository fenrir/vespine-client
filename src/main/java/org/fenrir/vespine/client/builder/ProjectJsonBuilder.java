package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.ProjectJsonDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class ProjectJsonBuilder 
{
	private JSONObject jsonObj;
	
	public ProjectJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IProjectDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String provider = jsonObj.getString("provider");
		String projectId = jsonObj.getString("projectId");
		String abbreviation = jsonObj.getString("abbreviation");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONArray subprojectList = jsonObj.getJSONArray("subprojects");
		JSONArray providerProjectList = jsonObj.getJSONArray("providerProjects");
		JSONObject workflow = jsonObj.getJSONObject("workflow");
		
		// Dades bàsiques
		ProjectJsonDTO dto = new ProjectJsonDTO();
		dto.setName(name);
		dto.setProvider(provider);
		dto.setProjectId(projectId);
		dto.setAbbreviation(abbreviation);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades subprojectes
		List<IProjectDTO> subprojectDTOList = new ArrayList<IProjectDTO>();
		for(int i=0; i<subprojectList.length(); i++){
			JSONObject subproject = subprojectList.getJSONObject(i);
			ProjectJsonBuilder builder = new ProjectJsonBuilder();
			builder.setJsonObj(subproject);
			subprojectDTOList.add(builder.build());
		}
		dto.setSubprojects(subprojectDTOList);
		
		// Dades workflow
		WorkflowJsonBuilder workflowBuilder = new WorkflowJsonBuilder();
		workflowBuilder.setJsonObj(workflow);
		dto.setWorkflow(workflowBuilder.build());
		
		// Dades provider elements
		List<IProviderElementDTO> providerElemDTOList = new ArrayList<IProviderElementDTO>();
		for(int i=0; i<providerProjectList.length(); i++){
			JSONObject elem = providerProjectList.getJSONObject(i);
			ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
			builder.setJsonObj(elem);
			providerElemDTOList.add(builder.build());
		}
		dto.setProviderProjects(providerElemDTOList);

		return dto;
	}
}
