package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.ProviderElementJsonDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
public class ProviderElementJsonBuilder 
{
	private JSONObject jsonObj;
	
	public ProviderElementJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IProviderElementDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String provider = jsonObj.getString("provider");
		String providerId = jsonObj.getString("providerId");
		String providerElementId = jsonObj.getString("providerElementId");
		String type = jsonObj.getString("type");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		ProviderElementJsonDTO dto = new ProviderElementJsonDTO();
		dto.setName(name);
		dto.setProvider(provider);
		dto.setProviderId(providerId);
		dto.setProviderElementId(providerElementId);
		dto.setType(type);
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
