package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.client.dto.SeverityJsonDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class SeverityJsonBuilder 
{
	private JSONObject jsonObj;
	
	public SeverityJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ISeverityDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String provider = jsonObj.getString("provider");
		String severity = jsonObj.getString("severityId");
		String slaTerm = jsonObj.getString("slaTerm");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONArray providerSeverityList = jsonObj.getJSONArray("providerSeverities");
		
		// Dades bàsiques
		SeverityJsonDTO dto = new SeverityJsonDTO();
		dto.setName(name);
		dto.setProvider(provider);
		dto.setSeverityId(severity);
		dto.setSlaTerm(slaTerm);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades provider elements
		List<IProviderElementDTO> providerElemDTOList = new ArrayList<IProviderElementDTO>();
		for(int i=0; i<providerSeverityList.length(); i++){
			JSONObject elem = providerSeverityList.getJSONObject(i);
			ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
			builder.setJsonObj(elem);
			providerElemDTOList.add(builder.build());
		}
		dto.setProviderSeverities(providerElemDTOList);
		
		return dto;
	}
}
