package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.SprintJsonDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class SprintJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(SprintJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public SprintJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ISprintDTO build() throws JSONException
	{
		String sprintId = jsonObj.getString("sprintId");
		String startDate = jsonObj.getString("startDate");
		String endDate = jsonObj.getString("endDate");
		JSONObject jsonProject = jsonObj.getJSONObject("project");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		/* Dades bàsiques */
		SprintJsonDTO dto = new SprintJsonDTO();
		dto.setSprintId(sprintId);
		try{
			dto.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse(startDate));
			dto.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse(endDate));
		}
		catch(ParseException e){
			log.error("Error interpretant d'inici / final de l'sprint {}: {}", new Object[]{sprintId, e.getMessage(), e});
		}
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades del projecte associat
		if(jsonProject!=null){
			ProjectJsonBuilder projectBuilder = new ProjectJsonBuilder();
			projectBuilder.setJsonObj(jsonProject);
			dto.setProject(projectBuilder.build());
		}
		
		return dto;
	}
}
