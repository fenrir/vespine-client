package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.StatusJsonDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class StatusJsonBuilder 
{
	private JSONObject jsonObj;
	
	public StatusJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IStatusDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String provider = jsonObj.getString("provider");
		String statusId = jsonObj.getString("statusId");
		String colorRGB = jsonObj.getString("colorRGB");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONArray providerStatusList = jsonObj.getJSONArray("providerStatus");
		
		// Dades bàsiques
		StatusJsonDTO dto = new StatusJsonDTO();
		dto.setName(name);
		dto.setProvider(provider);
		dto.setStatusId(statusId);
		dto.setColorRGB(colorRGB);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades provider elements
		List<IProviderElementDTO> providerElemDTOList = new ArrayList<IProviderElementDTO>();
		for(int i=0; i<providerStatusList.length(); i++){
			JSONObject elem = providerStatusList.getJSONObject(i);
			ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
			builder.setJsonObj(elem);
			providerElemDTOList.add(builder.build());
		}
		dto.setProviderStatus(providerElemDTOList);
		
		return dto;
	}
}
