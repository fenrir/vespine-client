package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.TagJsonDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
public class TagJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(TagJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public TagJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public ITagDTO build() throws JSONException
	{
		String tagId = jsonObj.getString("tagId");
		String name = jsonObj.getString("name");
		String strPreferred = jsonObj.getString("preferred");
		String creationDate = jsonObj.getString("creationDate");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		TagJsonDTO dto = new TagJsonDTO();
		dto.setTagId(tagId);
		dto.setName(name);
		dto.setPreferred(Boolean.valueOf(strPreferred));
		try{
			dto.setCreationDate(new SimpleDateFormat("yyyy-MM-dd").parse(creationDate));
		}
		catch(ParseException e){
			log.error("Error interpretant data de creació del l'etiqueta {}: {}", new Object[]{tagId, e.getMessage(), e});
		}
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
