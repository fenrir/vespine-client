package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.UserJsonDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class UserJsonBuilder 
{
	private JSONObject jsonObj;
	
	public UserJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IUserDTO build() throws JSONException
	{
		String provider = jsonObj.getString("provider");
		String userId = jsonObj.getString("userId");
		String username = jsonObj.getString("username");
		String completeName = jsonObj.getString("completeName");
		String active = jsonObj.getString("active");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		UserJsonDTO dto = new UserJsonDTO();
		dto.setProvider(provider);
		dto.setUserId(userId);
		dto.setCompleteName(completeName);
		dto.setUsername(username);
		dto.setActive(Boolean.valueOf(active));
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}