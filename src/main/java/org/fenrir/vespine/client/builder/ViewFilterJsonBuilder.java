package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.ViewFilterJsonDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class ViewFilterJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(ViewFilterJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public ViewFilterJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IViewFilterDTO build() throws JSONException
	{
		String filterId = jsonObj.getString("filterId");
		String name = jsonObj.getString("name");
		String description = jsonObj.getString("description");
		String filterQuery = jsonObj.getString("filterQuery");
		String orderClause = jsonObj.getString("orderClause");
		boolean applicationFilter = jsonObj.getBoolean("applicationFilter");
		long filterOrder = jsonObj.getLong("filterOrder");
		boolean transientFilter = jsonObj.getBoolean("transient");
		String creationDate = jsonObj.getString("creationDate");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		ViewFilterJsonDTO dto = new ViewFilterJsonDTO();
		dto.setFilterId(filterId);
		dto.setName(name);
		dto.setDescription(description);
		dto.setFilterQuery(filterQuery);
		dto.setOrderClause(orderClause);
		dto.setApplicationFilter(applicationFilter);
		dto.setFilterOrder(filterOrder);
		dto.setTransient(transientFilter);
		try{
			dto.setCreationDate(new SimpleDateFormat("yyyy-MM-dd").parse(creationDate));
		}
		catch(ParseException e){
			log.error("Error interpretant data de creació del filtre {}: {}", new Object[]{filterId, e.getMessage(), e});
		}
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
