package org.fenrir.vespine.client.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.client.dto.WorkRegistryJsonDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140813
 */
public class WorkRegistryJsonBuilder 
{
	private final Logger log = LoggerFactory.getLogger(WorkRegistryJsonBuilder.class);
	
	private JSONObject jsonObj;
	
	public WorkRegistryJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IWorkRegistryDTO build() throws JSONException
	{
		String registryId = jsonObj.getString("registryId");
		String description = !jsonObj.isNull("description") ? jsonObj.getString("description") : null;
		String workingDay = jsonObj.getString("workingDay");
//		JSONObject jsonIssue = jsonObj.getJSONObject("issue");
		JSONObject jsonSprint = jsonObj.optJSONObject("sprint");
		JSONObject jsonUser = jsonObj.getJSONObject("user");
		Integer time = jsonObj.getInt("time");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		
		WorkRegistryJsonDTO dto = new WorkRegistryJsonDTO();
		dto.setRegistryId(registryId);
		dto.setDescription(description);
		try{
			dto.setWorkingDay(new SimpleDateFormat("yyyy-MM-dd").parse(workingDay));
		}
		catch(ParseException e){
			log.error("Error interpretant d'inici / final de l'alerta {}: {}", new Object[]{registryId, e.getMessage(), e});
		}
//		IssueJsonBuilder issueBuilder = new IssueJsonBuilder();
//		dto.setIssue(issueBuilder.setJsonObj(jsonIssue).build());
		if(jsonSprint!=null){
			SprintJsonBuilder sprintBuilder = new SprintJsonBuilder();
			dto.setSprint(sprintBuilder.setJsonObj(jsonSprint).build());
		}
		UserJsonBuilder userBuilder = new UserJsonBuilder();
		dto.setUser(userBuilder.setJsonObj(jsonUser).build());
		dto.setTime(time);
		dto.setLastUpdated(new Date(lastUpdated));
		
		return dto;
	}
}
