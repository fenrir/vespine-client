package org.fenrir.vespine.client.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.WorkflowJsonDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class WorkflowJsonBuilder 
{
	private JSONObject jsonObj;
	
	public WorkflowJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IWorkflowDTO build() throws JSONException
	{
		String name = jsonObj.getString("name");
		String workflowId = jsonObj.getString("workflowId");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONObject initialStatus = jsonObj.getJSONObject("initialStatus");
		JSONArray stepList = jsonObj.getJSONArray("workflowSteps");
		
		// Dades bàsiques
		WorkflowJsonDTO dto = new WorkflowJsonDTO();
		dto.setName(name);
		dto.setWorkflowId(workflowId);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades estat inicial
		StatusJsonBuilder statusBuilder = new StatusJsonBuilder();
		statusBuilder.setJsonObj(initialStatus);
		dto.setInitialStatus(statusBuilder.build());
		
		// Dades steps
		List<IWorkflowStepDTO> stepDTOList = new ArrayList<IWorkflowStepDTO>();
		for(int i=0; i<stepList.length(); i++){
			JSONObject step = stepList.getJSONObject(i);
			WorkflowStepJsonBuilder builder = new WorkflowStepJsonBuilder();
			builder.setJsonObj(step);
			stepDTOList.add(builder.build());
		}
		dto.setWorkflowSteps(stepDTOList);
		
		return dto;
	}
}
