package org.fenrir.vespine.client.builder;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.client.dto.WorkflowStepJsonDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class WorkflowStepJsonBuilder 
{
	private JSONObject jsonObj;
	
	public WorkflowStepJsonBuilder setJsonObj(JSONObject jsonObj)
	{
		this.jsonObj = jsonObj;
		return this;
	}
	
	public IWorkflowStepDTO build() throws JSONException
	{
		String workflowStepId = jsonObj.getString("workflowStepId");
		Long lastUpdated = jsonObj.getLong("lastUpdated");
		JSONObject sourceStatus = jsonObj.getJSONObject("sourceStatus");
		JSONObject destinationStatus = jsonObj.getJSONObject("destinationStatus");
		
		// Dades bàsiques
		WorkflowStepJsonDTO dto = new WorkflowStepJsonDTO();
		dto.setWorkflowStepId(workflowStepId);
		dto.setLastUpdated(new Date(lastUpdated));
		
		// Dades estat origen
		StatusJsonBuilder statusBuilder = new StatusJsonBuilder();
		statusBuilder.setJsonObj(sourceStatus);
		dto.setSourceStatus(statusBuilder.build());
		
		// Dades estat destí
		statusBuilder = new StatusJsonBuilder();
		statusBuilder.setJsonObj(destinationStatus);
		dto.setDestinationStatus(statusBuilder.build());
		
		return dto;
	}
}
