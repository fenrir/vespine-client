package org.fenrir.vespine.client.dao;

import java.util.Collection;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.collections.map.MultiValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class WebResourceJsonDAO 
{
	private WebResource resource;
	
	public WebResourceJsonDAO(String endpoint)
	{
		resource = Client.create().resource(endpoint);
	}
	
	public JSONArray getJsonValues(String user, String password, String url) throws BusinessException
	{
		return getJsonValues(user, password, url, null);
	}
	
	public JSONArray getJsonValues(String user, String password, String url, MultiValueMap params) throws BusinessException
	{
		String response = getStringValue(user, password, url, params);
    	
		// Es recupera i es formata la resposta en format JSON
		try{
			JSONArray values = new JSONArray(response);
			return values;
		}
		catch(JSONException e){
			throw new BusinessException("Error obtenint resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	public JSONObject getSingleJsonValue(String user, String password, String url) throws BusinessException
	{
		return getSingleJsonValue(user, password, url, null);
	}
	
	public JSONObject getSingleJsonValue(String user, String password, String url, MultiValueMap params) throws BusinessException
	{
		String response = getStringValue(user, password, url, params);
    	
		// Es recupera i es formata la resposta en format JSON
		try{
			JSONObject value = new JSONObject(response);
			return value;
		}
		catch(JSONException e){
			throw new BusinessException("Error obtenint resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	public String getStringValue(String user, String password, String url) throws BusinessException
	{
		return getStringValue(user, password, url, null);
	}
	
	public String getStringValue(String user, String password, String url, MultiValueMap params) throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		WebResource pathResource = resource.path(url);
		if(params!=null && !params.isEmpty()){
			// Es passa la implementació de la llista de paràmetres cap a javax.ws.rs.core.MultivaluedMap
    		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
    		for(Object key:params.keySet()){
    			Collection<?> values = params.getCollection(key);
    			if(values!=null){
    				for(Object value:values){
    					queryParams.add((String)key, (String)value);
    				}
    			}
    		}
    		pathResource = pathResource.queryParams(queryParams);
		}
		
		ClientResponse response = pathResource
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(
						Base64.encode(user + ":" + password)))
				.get(ClientResponse.class);
		
		String strResponse = null;
		String faultCause = null;
		// OK
		if(response.getStatus()==Response.Status.OK.getStatusCode()){
			strResponse = response.getEntity(String.class);
		}
		// Error. En cas que l'error sigui un 204 - No Content, no es podrà recuperar la causa
		else if(response.getStatus()!=Response.Status.NO_CONTENT.getStatusCode()){
			faultCause = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()){
			throw new BusinessException("Usuari sense permisos de consulta: " + faultCause);
		}
		else if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error realitzant la consulta: " + faultCause);
		}
		// El codi 204 - No Content només es rebrà en consultes sobre un element simple
		else if(response.getStatus()==Response.Status.NO_CONTENT.getStatusCode()){
			throw new BusinessException("No s'ha trobat el recurs requerit");
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			throw new BusinessException("Error sense especificar realitzant la consulta: " + faultCause);	
		}
		
		return strResponse;		
	}
	
	public String create(String originator, String user, String password, String url, MultiValueMap params) throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		WebResource.Builder pathResource = resource.path(url)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encode(user + ":" + password)))
				.header("originator", originator);
		
		ClientResponse response;
		if(params!=null && !params.isEmpty()){
    		// Es passa la implementació de la llista de paràmetres cap a javax.ws.rs.core.MultivaluedMap
    		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
    		for(Object key:params.keySet()){
    			Collection<?> values = params.getCollection(key);
    			if(values!=null){
    				for(Object value:values){
    					queryParams.add((String)key, (String)value);
    				}
    			}
    		}
    		response = pathResource.post(ClientResponse.class, queryParams);
    	}
    	else{    	
    		response = pathResource.post(ClientResponse.class);
    	}
		
		String locationUrl = null;
		String faultCause = null;
		// OK
		if(response.getStatus()==Response.Status.CREATED.getStatusCode()){
			locationUrl = response.getHeaders().getFirst("Location");
		}
		// Error
		else{
			faultCause = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()){
			throw new BusinessException("Usuari sense permisos per crear el registre: " + faultCause);
		}
		else if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error creant el registre: " + faultCause);
		}
		else if(response.getStatus()!=Response.Status.CREATED.getStatusCode()){
			throw new BusinessException("Error sense especificar creant el registre: " + faultCause);	
		}
		
		return locationUrl;
	}
	
	public String update(String originator, String user, String password, String url, MultiValueMap params) throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		WebResource.Builder pathResource = resource.path(url)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encode(user + ":" + password)))
				.header("originator", originator);
		
		ClientResponse response;
		if(params!=null && !params.isEmpty()){
    		// Es passa la implementació de la llista de paràmetres cap a javax.ws.rs.core.MultivaluedMap
    		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
    		for(Object key:params.keySet()){
    			Collection<?> values = params.getCollection(key);
    			if(values!=null){
    				for(Object value:values){
    					queryParams.add((String)key, (String)value);
    				}
    			}
    		}
    		response = pathResource.put(ClientResponse.class, queryParams);
    	}
    	else{    	
    		response = pathResource.put(ClientResponse.class);
    	}
		
		String locationUrl = null;
		String faultCause = null;
		// OK
		if(response.getStatus()==Response.Status.OK.getStatusCode()){
			locationUrl = response.getHeaders().getFirst("Location");
		}
		// Error
		else{
			faultCause = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()){
			throw new BusinessException("Usuari sense permisos per modificar el registre: " + faultCause);
		}
		else if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error modificant el registre: " + faultCause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			throw new BusinessException("Error sense especificar modificant el registre: " + faultCause);	
		}
		
		return locationUrl;		
	}
	
	public void delete(String originator, String user, String password, String url) throws BusinessException
	{
		delete(originator, user, password, url, null);
	}
	
	public void delete(String originator, String user, String password, String url, MultiValueMap params) throws BusinessException
    {
    	if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
    	
    	WebResource.Builder pathResource = resource
				.path(url)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encode(user + ":" + password)))
				.header("originator", originator);
    	
    	ClientResponse response;
    	if(params!=null && !params.isEmpty()){
    		// Es passa la implementació de la llista de paràmetres cap a javax.ws.rs.core.MultivaluedMap
    		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
    		for(Object key:params.keySet()){
    			Collection<?> values = params.getCollection(key);
    			if(values!=null){
    				for(Object value:values){
    					queryParams.add((String)key, (String)value);
    				}
    			}
    		}
    		response = pathResource.delete(ClientResponse.class, queryParams);
    	}
    	else{    	
    		response = pathResource.delete(ClientResponse.class);
    	}
				
		String faultCause = null;
		// Error
		if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			faultCause = response.getEntity(String.class);
		}
		response.close();
		
		if(response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()){
			throw new BusinessException("Usuari sense permisos per esborrar el registre: " + faultCause);
		}
		else if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			throw new BusinessException("Error esborrant el registre: " + faultCause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			throw new BusinessException("Error sense especificar esborrant el registre: " + faultCause);	
		}
    }
}
