package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
public class AlertTypeJsonDTO implements IAlertTypeDTO 
{
	private String alertTypeId;
	private String name; 
	private String description;
	private String icon;
	private String alertRule;
	private Date creationDate;
	private Date lastUpdated;
	
	@Override
	public String getAlertTypeId() 
	{
		return alertTypeId;
	}
	
	public void setAlertTypeId(String alertTypeId)
	{
		this.alertTypeId = alertTypeId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public String getIcon() 
	{
		return icon;
	}
	
	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	@Override
	public String getAlertRule() 
	{
		return alertRule;
	}
	
	public void setAlertRule(String alertRule)
	{
		this.alertRule = alertRule;
	}

	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	public String toString()
	{
		return getName();
	}
	
	@Override
    public int hashCode()
    {
        return getAlertTypeId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IAlertTypeDTO)){
            return false;
        }

        IAlertTypeDTO other = (IAlertTypeDTO)obj;
        return getAlertTypeId().equals(other.getAlertTypeId());
    }
}
