package org.fenrir.vespine.client.dto;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IVespineCategoryDTO;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class CategoryJsonDTO implements IVespineCategoryDTO 
{
	private String name;
	private String provider;
	private String categoryId;
	private Date lastUpdated;
	private List<IProviderElementDTO> providerCategories;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	@Override
	public String getCategoryId() 
	{
		return categoryId;
	}
	
	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public IProjectDTO getProject() 
	{
		return null;
	}
	
	@Override
	public List<IProviderElementDTO> getProviderCategories() 
	{
		return providerCategories;
	}
	
	public void setProviderCategories(List<IProviderElementDTO> providerCategories)
	{
		this.providerCategories = providerCategories;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICategoryDTO)){
            return false;
        }
        
        final ICategoryDTO other = (ICategoryDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Category ID */
        if(getCategoryId()==null){
            if(other.getCategoryId()!=null){
                return false;
            }
        } 
        else if(!getCategoryId().equals(other.getCategoryId())){
            return false;
        }      
        
        return true;
    }
}
