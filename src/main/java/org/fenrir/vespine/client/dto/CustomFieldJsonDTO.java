package org.fenrir.vespine.client.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class CustomFieldJsonDTO implements IVespineCustomFieldDTO 
{
	private String fieldId;
	private String provider;
	private String dataProviderId;
	private String name;
	private IFieldType fieldType;
	private boolean mandatory;
	private Date lastUpdated;
	private List<?> projects;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	@Override
	public String getFieldId() 
	{
		return fieldId;
	}
	
	public void setFieldId(String fieldId)
	{
		this.fieldId = fieldId;
	}

	@Override
	public IFieldType getFieldType() 
	{
		return fieldType;
	}
	
	public void setFieldType(IFieldType fieldType)
	{
		this.fieldType = fieldType;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public boolean isMandatory() 
	{
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory)
	{
		this.mandatory = mandatory;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String getDataProviderId() 
	{
		return dataProviderId;
	}
	
	public void setDataProviderId(String dataProviderId)
	{
		this.dataProviderId = dataProviderId;
	}

	@Override
	public List<IVespineProjectDTO> getProjects() 
	{
		if(projects==null){
			return Collections.emptyList();
		}
		return (List<IVespineProjectDTO>)projects;
	}
	
	public void setProjects(List<?> projects)
	{
		this.projects = projects;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getFieldId() == null) ? 0 : getFieldId().hashCode());
        
        return result;
    }
	
	@Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICustomFieldDTO)){
            return false;
        }
        
        final ICustomFieldDTO other = (ICustomFieldDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Field ID */
        if(getFieldId()==null){
            if(other.getFieldId()!=null){
                return false;
            }
        } 
        else if(!getFieldId().equals(other.getFieldId())){
            return false;
        }      
        
        return true;
    }
}
