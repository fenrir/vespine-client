package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140717
 */
public class CustomValueJsonDTO implements ICustomValueDTO 
{
	private String valueId;
	private String provider;
	private ICustomFieldDTO field;
	private IIssueDTO issue;
	private String value;
	private Date lastUpdated;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	@Override
	public String getValueId() 
	{
		return valueId;
	}
	
	public void setValueId(String valueId)
	{
		this.valueId = valueId;
	}
	
	@Override
	public ICustomFieldDTO getField()
	{
		return field;
	}
	
	public void setField(ICustomFieldDTO field)
	{
		this.field = field;
	}
	
	@Override
	public IIssueDTO getIssue()
	{
		return issue;
	}
	
	public void setIssue(IIssueDTO issue)
	{
		this.issue = issue;
	}
	
	@Override
	public String getValue()
	{
		return value;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getValueId() == null) ? 0 : getValueId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICustomValueDTO)){
            return false;
        }
        
        final ICustomValueDTO other = (ICustomValueDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Value ID */
        if(getValueId()==null){
            if(other.getValueId()!=null){
                return false;
            }
        } 
        else if(!getValueId().equals(other.getValueId())){
            return false;
        }      
        
        return true;
    }
}
