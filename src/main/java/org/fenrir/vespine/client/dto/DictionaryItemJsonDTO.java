package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryItemJsonDTO implements IDictionaryItemDTO
{
	private String itemId;
	private String description;
	private String value;
	private Date lastUpdated;
	
	@Override
	public String getItemId() 
	{
		return itemId;
	}
	
	public void setItemId(String itemId) 
	{
		this.itemId = itemId;
	}
	
	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}
	
	@Override
	public IDictionaryDTO getDictionary()
	{
		return null;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return "[" + value + "] " + description;
	}
	
	@Override
    public int hashCode()
    {
        return getItemId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IDictionaryItemDTO)){
            return false;
        }

        IDictionaryItemDTO other = (IDictionaryItemDTO)obj;
        return getItemId().equals(other.getItemId());
    }
}
