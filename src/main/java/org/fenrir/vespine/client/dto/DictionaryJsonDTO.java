package org.fenrir.vespine.client.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryJsonDTO implements IDictionaryDTO 
{
	private String dictionaryId;
	private String name;
	private String description;
	private List<IDictionaryItemDTO> dictionaryItems;
	private Date lastUpdated;
	
	@Override
	public String getDictionaryId() 
	{
		return dictionaryId;
	}
	
	public void setDictionaryId(String dictionaryId) 
	{
		this.dictionaryId = dictionaryId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public List<IDictionaryItemDTO> getDictionaryItems() 
	{
		if(dictionaryItems==null){
			return Collections.emptyList();
		}
		return dictionaryItems;
	}
	
	public void setDictionaryItems(List<IDictionaryItemDTO> dictionaryItems) 
	{
		this.dictionaryItems = dictionaryItems;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return getName();
	}
	
	@Override
    public int hashCode()
    {
        return getDictionaryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IDictionaryDTO)){
            return false;
        }

        IDictionaryDTO other = (IDictionaryDTO)obj;
        return getDictionaryId().equals(other.getDictionaryId());
    }
}
