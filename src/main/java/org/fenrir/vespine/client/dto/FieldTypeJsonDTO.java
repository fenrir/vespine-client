package org.fenrir.vespine.client.dto;

import java.util.Collections;
import java.util.Map;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class FieldTypeJsonDTO implements IFieldType 
{
	private String type;
	private Class<? extends IAttributeConverter<?, ?>> converter;
	private Map<String, String> converterParameters;
	private boolean dataProviderId;
	
	@Override
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}

	@Override
	public Class<? extends IAttributeConverter<?, ?>> getConverter() 
	{
		return converter;
	}
	
	public void setConverter(Class<? extends IAttributeConverter<?, ?>> converter)
	{
		this.converter = converter;
	}

	@Override
	public Map<String, String> getConverterParameters() 
	{
		if(converterParameters==null){
			return Collections.emptyMap();
		}
		return converterParameters;
	}
	
	public void setConverterParameters(Map<String, String> converterParemeters)
	{
		this.converterParameters = converterParemeters;
	}

	@Override
	public boolean needDataProviderId() 
	{
		return dataProviderId;
	}
	
	public void setDataProviderId(boolean dataProviderId)
	{
		this.dataProviderId = dataProviderId;
	}
	
	@Override
	public String toString()
	{
		return type;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj){
			return true;
		}
		if(obj == null){
			return false;
		}
		if(getClass() != obj.getClass()){
			return false;
		}
		
		IFieldType other = (IFieldType) obj;
		if(type==null){
			if(other.getType()!=null){
				return false;
			}
		}
		else if(!type.equals(other.getType())){
			return false;
		}
		
		return true;
	}
}
