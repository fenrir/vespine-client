package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueAlertJsonDTO implements IIssueAlertDTO
{
	private String alertId;
	private IIssueDTO issue;
	private IAlertTypeDTO type;
	private Date creationDate;
	
	@Override
	public String getAlertId() 
	{
		return alertId;
	}
	
	public void setAlertId(String alertId) 
	{
		this.alertId = alertId;
	}
	
	@Override
	public IIssueDTO getIssue() 
	{
		return issue;
	}
	
	public void setIssue(IIssueDTO issue) 
	{
		this.issue = issue;
	}
	
	@Override
	public IAlertTypeDTO getType() 
	{
		return type;
	}
	
	public void setType(IAlertTypeDTO type)
	{
		this.type = type;
	}
	
	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) 
	{
		this.creationDate = creationDate;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getAlertId() == null) ? 0 : getAlertId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IIssueAlertDTO other = (IIssueAlertDTO) obj;
		if (getAlertId() == null) {
			if (other.getAlertId() != null)
				return false;
		} else if (!getAlertId().equals(other.getAlertId()))
			return false;
		return true;
	}
}
