package org.fenrir.vespine.client.dto;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueJsonDTO implements IVespineIssueDTO 
{
	private String provider;
	private String issueId;
	private String visibleId;
	private String summary;
	private String description;
	private IStatusDTO status;
	private IProjectDTO project;
	private ISeverityDTO severity;
	private ICategoryDTO category;
	private IUserDTO reporter;
	private IUserDTO assignedUser;
	private Date sendDate;
	private Date resolutionDate;
	private Date modifiedDate;
	private Boolean sla;
    private Date slaDate;
    private Integer estimatedTime;
    private String estimatedTimeFormated;
    private List<ITagDTO> tags;
    private List<IIssueNoteDTO> notes;
    private Date lastUpdated;
    private ISprintDTO sprint;
    private List<IWorkRegistryDTO>workRegistries;
    private Integer workTime;
    private String workTimeFormated;
    private boolean deleted;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider) 
	{
		this.provider = provider;
	}
	
	@Override
	public String getIssueId()
	{
		return issueId;
	}
	
	public void setIssueId(String issueId)
	{
		this.issueId = issueId;
	}
	
	@Override
	public String getVisibleId()
	{
		return visibleId;
	}
	
	public void setVisibleId(String visibleId)
	{
		this.visibleId = visibleId;
	}
	
	@Override
	public String getSummary() 
	{
		return summary;
	}
	
	public void setSummary(String summary)
	{
		this.summary = summary;
	}
	
	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public IStatusDTO getStatus() 
	{
		return status;
	}
	
	public void setStatus(IStatusDTO status) 
	{
		this.status = status;
	}
	
	@Override
	public IProjectDTO getProject() 
	{
		return project;
	}
	
	public void setProject(IProjectDTO project) 
	{
		this.project = project;
	}
	
	@Override
	public ISeverityDTO getSeverity()
	{
		return severity;
	}
	
	public void setSeverity(ISeverityDTO severity) 
	{
		this.severity = severity;
	}
	
	@Override
	public ICategoryDTO getCategory()
	{
		return category;
	}
	
	public void setCategory(ICategoryDTO category)
	{
		this.category = category;
	}
	
	@Override
	public IUserDTO getReporter() 
	{
		return reporter;
	}
	
	public void setReporter(IUserDTO reporter)
	{
		this.reporter = reporter;
	}
	
	@Override
	public IUserDTO getAssignedUser() 
	{
		return assignedUser;
	}
	
	public void setAssignedUser(IUserDTO assignedUser) 
	{
		this.assignedUser = assignedUser;
	}
	
	@Override
	public Date getSendDate() 
	{
		return sendDate;
	}
	
	public void setSendDate(Date sendDate) 
	{
		this.sendDate = sendDate;
	}
	
	@Override
	public Date getResolutionDate()
	{
		return resolutionDate;
	}
	
	public void setResolutionDate(Date resolutionDate) 
	{
		this.resolutionDate = resolutionDate;
	}
	
	@Override
	public Date getModifiedDate() 
	{
		return modifiedDate;
	}
	
	public void setModifiedDate(Date modifiedDate) 
	{
		this.modifiedDate = modifiedDate;
	}
	
	public Boolean isSla() 
	{
		return sla;
	}
	
	public void setSla(Boolean sla) 
	{
		this.sla = sla;
	}
	
	@Override
	public Date getSlaDate()
	{
		return slaDate;
	}
	
	public void setSlaDate(Date slaDate)
	{
		this.slaDate = slaDate;
	}
	
	@Override
	public Integer getEstimatedTime() 
	{
		return estimatedTime;
	}
	
	public void setEstimatedTime(Integer estimatedTime)
	{
		this.estimatedTime = estimatedTime;
	}
	
	@Override
	public String getEstimatedTimeFormated()
	{
		return estimatedTimeFormated;
	}
	
	public void setEstimatedTimeFormated(String estimatedTimeFormated) 
	{
		this.estimatedTimeFormated = estimatedTimeFormated;
	}
	
	@Override
	public List<ITagDTO> getTags() {
		return tags;
	}
	
	public void setTags(List<ITagDTO> tags)
	{
		this.tags = tags;
	}
	
	@Override
	public List<IIssueNoteDTO> getNotes()
	{
		return notes;
	}
	
	public void setNotes(List<IIssueNoteDTO> notes)
	{
		this.notes = notes;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public ISprintDTO getSprint()
	{
		return sprint;
	}
	
	public void setSprint(ISprintDTO sprint) 
	{
		this.sprint = sprint;
	}
	
	@Override
	public List<IWorkRegistryDTO> getWorkRegistries()
	{
		return workRegistries;
	}
	
	public void setWorkRegistries(List<IWorkRegistryDTO> workRegistries)
	{
		this.workRegistries = workRegistries;
	}
	
	@Override
	public Integer getWorkTime()
	{
		return workTime;
	}
	
	public void setWorkTime(Integer workTime)
	{
		this.workTime = workTime;
	}
	
	@Override
	public String getWorkTimeFormated() 
	{
		return workTimeFormated;
	}
	
	public void setWorkTimeFormated(String workTimeFormated)
	{
		this.workTimeFormated = workTimeFormated;
	}
	
	@Override
	public boolean isDeleted() 
	{
		return deleted;
	}
	
	public void setDeleted(boolean deleted) 
	{
		this.deleted = deleted;
	}
	
	public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getIssueId() == null) ? 0 : getIssueId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueDTO)){
            return false;
        }
        
        final IIssueDTO other = (IIssueDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Issue ID */
        if(getIssueId()==null){
            if(other.getIssueId()!=null){
                return false;
            }
        } 
        else if(!getIssueId().equals(other.getIssueId())){
            return false;
        }      
        
        return true;
    }
}
