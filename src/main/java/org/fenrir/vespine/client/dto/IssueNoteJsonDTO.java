package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueNoteJsonDTO implements IIssueNoteDTO 
{
	private String noteId;
	private String provider;
	private IIssueDTO issue;
	private String reporter;
	private String text;    
	private Date sendDate;
	private Date lastEditionDate;

	@Override
	public String getNoteId() 
	{
		return noteId;
	}
	
	public void setNoteId(String noteId)
	{
		this.noteId = noteId;
	}

	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	@Override
	public IIssueDTO getIssue() 
	{
		return issue;
	}
	
	public void setIssue(IIssueDTO issue) 
	{
		this.issue = issue;
	}

	@Override
	public String getReporter() 
	{
		return reporter;
	}
	
	public void setReporter(String reporter)
	{
		this.reporter = reporter;
	}

	@Override
	public String getText() {
		return text;
	}
	
	public void setText(String text) 
	{
		this.text = text;
	}

	@Override
	public Date getSendDate() 
	{
		return sendDate;
	}
	public void setSendDate(Date sendDate) 
	{
		this.sendDate = sendDate;
	}

	@Override
	public Date getLastEditionDate() 
	{
		return lastEditionDate;
	}
	
	public void setLastEditionDate(Date lastEditionDate)
	{
		this.lastEditionDate = lastEditionDate;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getNoteId() == null) ? 0 : getNoteId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueNoteDTO)){
            return false;
        }
        
        final IIssueNoteDTO other = (IIssueNoteDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Note ID */
        if(getNoteId()==null){
            if(other.getNoteId()!=null){
                return false;
            }
        } 
        else if(!getNoteId().equals(other.getNoteId())){
            return false;
        }      
        
        return true;
    }
}
