package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueWipRegistryJsonDTO implements IIssueWipRegistryDTO 
{
	private String registryId;
	private IIssueDTO issue;
	private IUserDTO user;
	private Date lastUpdated;
	
	@Override
	public String getRegistryId()
	{
		return registryId;
	}
	
	public void setRegistryId(String registryId) 
	{
		this.registryId = registryId;
	}
	
	@Override
	public IIssueDTO getIssue()
	{
		return issue;
	}
	
	public void setIssue(IIssueDTO issue) 
	{
		this.issue = issue;
	}
	
	@Override
	public IUserDTO getUser() 
	{
		return user;
	}
	public void setUser(IUserDTO user)
	{
		this.user = user;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public int hashCode()
    {
        return getRegistryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueWipRegistryDTO)){
            return false;
        }

        IIssueWipRegistryDTO other = (IIssueWipRegistryDTO)obj;
        return getRegistryId().equals(other.getRegistryId());
    }
}
