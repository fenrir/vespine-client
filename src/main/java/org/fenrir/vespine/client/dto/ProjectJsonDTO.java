package org.fenrir.vespine.client.dto;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class ProjectJsonDTO implements IVespineProjectDTO
{
	private String name;
	private String provider;
	private String projectId;
	private String abbreviation;
	private Date lastUpdated;
	private List<IProjectDTO> subprojects;
	private List<IProviderElementDTO> providerProjects;
	private IWorkflowDTO workflow;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	@Override
	public String getProjectId() 
	{
		return projectId;
	}
	
	public void setProjectId(String projectId)
	{
		this.projectId = projectId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public String getAbbreviation() 
	{
		return abbreviation;
	}
	
	public void setAbbreviation(String abbreviation)
	{
		this.abbreviation = abbreviation;
	}
	
	@Override
	public IProjectDTO getParentProject() 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<IProjectDTO> getSubprojects() 
	{
		return subprojects;
	}
	
	public void setSubprojects(List<IProjectDTO> subprojects)
	{
		this.subprojects = subprojects;
	}
	
	@Override
	public IWorkflowDTO getWorkflow()
	{
		return workflow;
	}
	
	public void setWorkflow(IWorkflowDTO workflow)
	{
		this.workflow = workflow;
	}
	
	@Override
	public List<IProviderElementDTO> getProviderProjects() 
	{
		return providerProjects;
	}
	
	public void setProviderProjects(List<IProviderElementDTO> providerProjects)
	{
		this.providerProjects = providerProjects;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getProjectId() == null) ? 0 : getProjectId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IProjectDTO)){
            return false;
        }
        
        final IProjectDTO other = (IProjectDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Project ID */
        if(getProjectId()==null){
            if(other.getProjectId()!=null){
                return false;
            }
        } 
        else if(!getProjectId().equals(other.getProjectId())){
            return false;
        }      
        
        return true;
    }
}
