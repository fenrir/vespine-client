package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
public class ProviderElementJsonDTO implements IProviderElementDTO 
{
	private String type;
	private String providerElementId;
	private String name;
	private String provider;
	private String providerId;
	private Date lastUpdated;
	
	@Override
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	@Override
	public String getProviderElementId() 
	{
		return providerElementId;
	}
	
	public void setProviderElementId(String providerElementId) 
	{
		this.providerElementId = providerElementId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider) 
	{
		this.provider = provider;
	}
	
	@Override
	public String getProviderId() 
	{
		return providerId;
	}
	
	public void setProviderId(String providerId) 
	{
		this.providerId = providerId;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getProviderId() == null) ? 0 : getProviderId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IProviderElementDTO)){
            return false;
        }
        
        final IProviderElementDTO other = (IProviderElementDTO)obj;
        /* Type */
        if(getType()==null){
        	if(other.getType()!=null){
        		return false;
        	}
        }
        else if(!getType().equals(other.getType())){
        	return false;
        }
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Provider ID */
        if(getProviderId()==null){
            if(other.getProviderId()!=null){
                return false;
            }
        } 
        else if(!getProviderId().equals(other.getProviderId())){
            return false;
        }      
        
        return true;
    }
}
