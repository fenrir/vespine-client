package org.fenrir.vespine.client.dto;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IVespineSeverityDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class SeverityJsonDTO implements IVespineSeverityDTO 
{

	private String name;
	private String provider;
	private String severityId;
	private String slaTerm;
	private Date lastUpdated;
	private List<IProviderElementDTO> providerSeverities;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	@Override
	public String getSeverityId() 
	{
		return severityId;
	}
	
	public void setSeverityId(String severityId)
	{
		this.severityId = severityId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getSlaTerm() 
	{
		return slaTerm;
	}
	
	public void setSlaTerm(String slaTerm)
	{
		this.slaTerm = slaTerm;
	}

	@Override
	public List<IProviderElementDTO> getProviderSeverities() 
	{
		return providerSeverities;
	}
	
	public void setProviderSeverities(List<IProviderElementDTO> providerSeverities)
	{
		this.providerSeverities = providerSeverities;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getSeverityId() == null) ? 0 : getSeverityId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISeverityDTO)){
            return false;
        }
        
        final ISeverityDTO other = (ISeverityDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Severity ID */
        if(getSeverityId()==null){
            if(other.getSeverityId()!=null){
                return false;
            }
        } 
        else if(!getSeverityId().equals(other.getSeverityId())){
            return false;
        }      
        
        return true;
    }
}
