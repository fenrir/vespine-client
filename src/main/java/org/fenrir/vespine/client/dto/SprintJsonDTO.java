package org.fenrir.vespine.client.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class SprintJsonDTO implements ISprintDTO 
{
	private String sprintId;
	private IProjectDTO project;
	private Date startDate;
	private Date endDate;
	private Date lastUpdated;
	
	public String getSprintId() 
	{
		return sprintId;
	}
	
	public void setSprintId(String sprintId) 
	{
		this.sprintId = sprintId;
	}
	
	public IProjectDTO getProject() 
	{
		return project;
	}
	
	public void setProject(IProjectDTO project) 
	{		
		this.project = project;
	}
	
	public Date getStartDate() 
	{
		return startDate;
	}
	
	public void setStartDate(Date startDate) 
	{
		this.startDate = startDate;
	}
	
	public Date getEndDate() 
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate) 
	{
		this.endDate = endDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	public String toString()
	{
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	StringBuilder sb = new StringBuilder();
    	return sb.append("[").append(project).append("] ")
			.append(df.format(startDate))
    		.append(" - ")
    		.append(df.format(endDate)).toString();
	}
	
	@Override
    public int hashCode()
    {
        return getSprintId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISprintDTO)){
            return false;
        }

        ISprintDTO other = (ISprintDTO)obj;
        return getSprintId().equals(other.getSprintId());
    }
}
