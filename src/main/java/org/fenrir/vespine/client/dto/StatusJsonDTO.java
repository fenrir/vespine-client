package org.fenrir.vespine.client.dto;

import java.util.Date;
import java.util.List;

import org.fenrir.vespine.core.dto.IVespineStatusDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class StatusJsonDTO implements IVespineStatusDTO
{
	private String name;
	private String provider;
	private String statusId;
	private String colorRGB;
	private Date lastUpdated;
	private List<IProviderElementDTO> providerStatus;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	@Override
	public String getStatusId() 
	{
		return statusId;
	}
	
	public void setStatusId(String statusId)
	{
		this.statusId = statusId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public String getColorRGB() 
	{
		return colorRGB;
	}
	
	public void setColorRGB(String colorRGB)
	{
		this.colorRGB = colorRGB;
	}
	
	@Override
	public List<IProviderElementDTO> getProviderStatus() 
	{
		return providerStatus;
	}
	
	public void setProviderStatus(List<IProviderElementDTO> providerStatus)
	{
		this.providerStatus = providerStatus;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getStatusId() == null) ? 0 : getStatusId().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IStatusDTO)){
            return false;
        }

        IStatusDTO other = (IStatusDTO) obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Status Id */
        if(getStatusId()==null){
            if(other.getStatusId()!=null){
                return false;
            }
        }
        else if(!getStatusId().equals(other.getStatusId())){
            return false;
        }

        return true;
    }
}
