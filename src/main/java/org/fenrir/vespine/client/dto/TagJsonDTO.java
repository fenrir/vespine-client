package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
public class TagJsonDTO implements ITagDTO 
{
	private String tagId;
	private String name;
	private Boolean preferred;
	private Date creationDate;
	private Date lastUpdated;
	
	@Override
	public String getTagId() 
	{
		return tagId;
	}
	
	public void setTagId(String tagId)
	{
		this.tagId = tagId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public Boolean isPreferred() 
	{
		return preferred;
	}
	
	public void setPreferred(Boolean preferred)
	{
		this.preferred = preferred;
	}

	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	public String toString()
	{
		return getName();
	}
	
	@Override
    public int hashCode()
    {
        return getTagId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ITagDTO)){
            return false;
        }

        ITagDTO other = (ITagDTO)obj;
        return getTagId().equals(other.getTagId());
    }
}
