package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
public class UserJsonDTO implements IUserDTO 
{
	private String provider;
	private String userId;
	private String username;
	private String completeName;
	private Boolean active; 
	private Date lastUpdated;
	
	@Override
	public String getProvider() 
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	@Override
	public String getUserId() 
	{
		return userId;
	}
	
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@Override
	public String getUsername() 
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}

	@Override
	public String getCompleteName() 
	{
		return completeName;
	}
	
	public void setCompleteName(String completeName)
	{
		this.completeName = completeName;
	}

	@Override
	public Boolean isActive() 
	{
		return active;
	}
	
	public void setActive(Boolean active)
	{
		this.active = active;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return completeName;
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IUserDTO)){
            return false;
        }
        
        final IUserDTO other = (IUserDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* User ID */
        if(getUserId()==null){
            if(other.getUserId()!=null){
                return false;
            }
        } 
        else if(!getUserId().equals(other.getUserId())){
            return false;
        }      
        
        return true;
    }
}
