package org.fenrir.vespine.client.dto;

import java.util.Date;

import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class ViewFilterJsonDTO implements IViewFilterDTO
{
	private String filterId;
	private String name;
	private String description;
	private String filterQuery;
	private String orderClause;
	private boolean applicationFilter;
	private long filterOrder;
	private boolean transientFilter;
	private Date creationDate;
	private Date lastUpdated;
	
	@Override
	public String getFilterId() 
	{
		return filterId;
	}
	
	public void setFilterId(String filterId) 
	{
		this.filterId = filterId;
	}
	
	@Override
	public String getName()
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public String getFilterQuery() 
	{
		return filterQuery;
	}
	
	public void setFilterQuery(String filterQuery)
	{
		this.filterQuery = filterQuery;
	}
	
	@Override
	public String getOrderClause()
	{
		return orderClause;
	}
	
	public void setOrderClause(String orderClause)
	{
		this.orderClause = orderClause;
	}
	
	@Override
	public Boolean isApplicationFilter() 
	{
		return applicationFilter;
	}
	
	public void setApplicationFilter(boolean applicationFilter) 
	{
		this.applicationFilter = applicationFilter;
	}
	
	@Override
	public Long getFilterOrder() 
	{
		return filterOrder;
	}
	
	public void setFilterOrder(long filterOrder) 
	{
		this.filterOrder = filterOrder;
	}
	
	@Override
	public Boolean isTransient() 
	{
		return transientFilter;
	}
	
	public void setTransient(boolean transientFilter) 
	{
		this.transientFilter = transientFilter;
	}
	
	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode()
    {
        return getFilterId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISprintDTO)){
            return false;
        }

        IViewFilterDTO other = (IViewFilterDTO)obj;
        return getFilterId().equals(other.getFilterId());
    }
}
