package org.fenrir.vespine.client.dto;

import java.util.Date;

import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class WorkRegistryJsonDTO implements IWorkRegistryDTO
{
	private String registryId;
	private IIssueDTO issue;
	private String description;
	private ISprintDTO sprint;
	private IUserDTO user;
	private Date workingDay;
	private Integer time;
	private Date lastUpdated;
	
	@Override
	public String getRegistryId() 
	{
		return registryId;
	}
	
	public void setRegistryId(String registryId)
	{
		this.registryId = registryId;
	}
	
	@Override
	public IIssueDTO getIssue() 
	{
		return issue;
	}
	
	public void setIssue(IIssueDTO issue)
	{
		this.issue = issue;
	}
	
	@Override
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Override
	public ISprintDTO getSprint()
	{
		return sprint;
	}
	
	public void setSprint(ISprintDTO sprint)
	{
		this.sprint = sprint;
	}
	
	@Override
	public IUserDTO getUser()
	{
		return user;
	}
	
	public void setUser(IUserDTO user) 
	{
		this.user = user;
	}
	
	@Override
	public Date getWorkingDay() 
	{
		return workingDay;
	}
	
	public void setWorkingDay(Date workingDay)
	{
		this.workingDay = workingDay;
	}
	
	@Override
	public Integer getTime()
	{
		return time;
	}
	
	public void setTime(Integer time) 
	{
		this.time = time;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public int hashCode()
    {
        return getRegistryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkRegistryDTO)){
            return false;
        }

        IWorkRegistryDTO other = (IWorkRegistryDTO)obj;
        return getRegistryId().equals(other.getRegistryId());
    }
}
