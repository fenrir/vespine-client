package org.fenrir.vespine.client.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class WorkflowJsonDTO implements IWorkflowDTO
{
	private String workflowId;
	private String name;
	private IStatusDTO initialStatus;
	private List<IWorkflowStepDTO> workflowSteps;
	private Date lastUpdated;
	
	@Override
	public String getWorkflowId() 
	{
		return workflowId;
	}
	
	public void setWorkflowId(String workflowId) 
	{
		this.workflowId = workflowId;
	}
	
	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	@Override
	public IStatusDTO getInitialStatus() 
	{
		return initialStatus;
	}
	
	public void setInitialStatus(IStatusDTO initialStatus) 
	{
		this.initialStatus = initialStatus;
	}
	
	@Override
	public List<IWorkflowStepDTO> getWorkflowSteps() 
	{
		if(workflowSteps==null){
			return Collections.emptyList();
		}
		return workflowSteps;
	}
	
	public void setWorkflowSteps(List<IWorkflowStepDTO> workflowSteps) 
	{
		this.workflowSteps = workflowSteps;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
    public int hashCode()
    {
        return getWorkflowId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkflowDTO)){
            return false;
        }

        IWorkflowDTO other = (IWorkflowDTO)obj;
        return getWorkflowId().equals(other.getWorkflowId());
    }
}
