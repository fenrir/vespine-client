package org.fenrir.vespine.client.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140707
 */
public class WorkflowStepJsonDTO implements IWorkflowStepDTO
{
	private String workflowStepId;
	private IStatusDTO sourceStatus;
	private IStatusDTO destinationStatus;
	private Date lastUpdated;
	
	@Override
	public String getWorkflowStepId() 
	{
		return workflowStepId;
	}
	
	public void setWorkflowStepId(String workflowStepId) 
	{
		this.workflowStepId = workflowStepId;
	}
	
	@Override
	public IWorkflowDTO getWorkflow()
	{
		return null;
	}
	
	@Override
	public IStatusDTO getSourceStatus() 
	{
		return sourceStatus;
	}
	
	public void setSourceStatus(IStatusDTO sourceStatus) 
	{
		this.sourceStatus = sourceStatus;
	}
	
	@Override
	public IStatusDTO getDestinationStatus() 
	{
		return destinationStatus;
	}
	
	public void setDestinationStatus(IStatusDTO destinationStatus) 
	{
		this.destinationStatus = destinationStatus;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public int hashCode()
    {
        return getWorkflowStepId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkflowDTO)){
            return false;
        }

        IWorkflowStepDTO other = (IWorkflowStepDTO)obj;
        return getWorkflowStepId().equals(other.getWorkflowStepId());
    }
}
