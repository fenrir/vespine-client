package org.fenrir.vespine.client.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

import org.fenrir.vespine.client.application.service.IApplicationService;
import org.fenrir.vespine.client.application.service.impl.ApplicationServiceImpl;
import org.fenrir.vespine.client.authentication.service.IAuthenticationService;
import org.fenrir.vespine.client.authentication.service.impl.AuthenticationServiceImpl;
import org.fenrir.vespine.client.service.impl.AdministrationFacadeRESTImpl;
import org.fenrir.vespine.client.service.impl.IssueFacadeRESTImpl;
import org.fenrir.vespine.client.service.impl.IssueWorkRESTFacadeImpl;
import org.fenrir.vespine.client.service.impl.SearchIndexRESTFacadeImpl;
import org.fenrir.vespine.client.service.impl.UserRESTFacadeImpl;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.core.service.IIssueWorkFacade;
import org.fenrir.vespine.core.service.ISearchIndexFacade;
import org.fenrir.vespine.core.service.IUserFacade;
import org.fenrir.vespine.spi.ExtensionConstants;
import org.fenrir.vespine.spi.builder.IIssueURLBuilder;
import org.fenrir.vespine.spi.service.IIssueDataService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140722
 */
public class VespineClientModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		bind(IAdministrationFacade.class).to(AdministrationFacadeRESTImpl.class).in(Singleton.class);
		bind(IIssueFacade.class).to(IssueFacadeRESTImpl.class).in(Singleton.class);
		bind(IIssueWorkFacade.class).to(IssueWorkRESTFacadeImpl.class).in(Singleton.class);
		bind(ISearchIndexFacade.class).to(SearchIndexRESTFacadeImpl.class).in(Singleton.class);
		bind(IUserFacade.class).to(UserRESTFacadeImpl.class).in(Singleton.class);
		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class).in(Singleton.class);
		bind(IApplicationService.class).to(ApplicationServiceImpl.class).in(Singleton.class);
		
		/* Bindings per les extensions. 
         * Si no s'inicialitzen es produeix un error en el moment en que es demana l'injecció de l'objecte.
         * Si no hi ha cap objecte configurat amb la key especificada, s'injectarà un Set buit  
         */
		Multibinder.newSetBinder(binder(), String.class, Names.named(ExtensionConstants.EXTENSION_PROVIDER_BINDING_ID));
		Multibinder.newSetBinder(binder(), IIssueDataService.class);
		Multibinder.newSetBinder(binder(), IIssueURLBuilder.class);
	}
}
