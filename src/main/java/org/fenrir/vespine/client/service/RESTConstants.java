package org.fenrir.vespine.client.service;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140807
 */
public class RESTConstants 
{
	/*---------------------------------*
     *           Aplicació             *
     *---------------------------------*/
	public static final String URL_APPLICATION_LOGIN = "/rest/authentication/login";
	public static final String URL_API = "/rest/application/api.json";
	
	/*---------------------------------*
     *           Broadcast             *
     *---------------------------------*/
	public static final String URL_BROADCAST_SUBSCRIBE = "/broadcast/subscribe";
	
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
	public static final String URL_PROJECT = "/rest/project";
	public static final String URL_PROJECTS_JSON = URL_PROJECT + "/search.json";
	public static final String URL_PROJECT_JSON = URL_PROJECT + "/{0}.json";
	public static final String URL_PROJECT_ISSUES_COUNT = URL_PROJECT + "/{0}/issues.count";

	/*---------------------------------*
     *           Categories            *
     *---------------------------------*/
	public static final String URL_CATEGORY = "/rest/category";
	public static final String URL_CATEGORIES_JSON = URL_CATEGORY + "/search.json";
	public static final String URL_CATEGORY_JSON = URL_CATEGORY + "/{0}.json";
	public static final String URL_CATEGORY_ISSUES_COUNT = URL_CATEGORY + "/{0}/issues.count";

	/*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
	public static final String URL_SEVERITY = "/rest/severity";
	public static final String URL_SEVERITIES_JSON = URL_SEVERITY + "/search.json";
	public static final String URL_SEVERITY_JSON = URL_SEVERITY + "/{0}.json";
	public static final String URL_SEVERITY_ISSUES_COUNT = URL_SEVERITY + "/{0}/issues.count";

	/*---------------------------------*
     *             Estats              *
     *---------------------------------*/
	public static final String URL_STATUS = "/rest/status";
	public static final String URL_STATUS_ALL_JSON = URL_STATUS + "/search.json";
	public static final String URL_STATUS_JSON = URL_STATUS + "/{0}.json";
	public static final String URL_STATUS_NEXT_JSON = URL_STATUS + "/next.json";
	public static final String URL_STATUS_COLOR = URL_STATUS + "/{0}/color";
	public static final String URL_STATUS_ISSUES_COUNT = URL_STATUS + "/{0}/issues.count";

	/*---------------------------------*
     *           Workflow              *
     *---------------------------------*/
	public static final String URL_WORKFLOW = "/rest/workflow";
	public static final String URL_WORKFLOWS_JSON = URL_WORKFLOW + "/search.json";
	public static final String URL_WORKFLOW_JSON = URL_WORKFLOW + "/{0}.json";
	public static final String URL_WORKFLOW_PROJECTS_JSON = URL_WORKFLOW + "/{0}/projects.json";

	/*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
	public static final String URL_ALERT_TYPE = "/rest/alertType";
	public static final String URL_ALERT_TYPES_JSON = URL_ALERT_TYPE + "/search.json";
	public static final String URL_ALERT_TYPE_JSON = URL_ALERT_TYPE + "/{0}.json";
	
	/*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
	public static final String URL_ISSUE_ALERT = "/rest/issue/{0}/alert";
	public static final String URL_ISSUE_ALERTS_JSON = "/rest/issue/{0}/alerts.json";
	public static final String URL_ALERTS_BY_TYPE_JSON = URL_ALERT_TYPE + "/{0}/alerts.json";
	public static final String URL_ALERTS_BY_TYPE = URL_ALERT_TYPE + "/{0}/alerts";

	/*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
	public static final String URL_DICTIONARY = "/rest/dictionary";
	public static final String URL_DICTIONARIES_JSON = URL_DICTIONARY + "/search.json";
	public static final String URL_DICTIONARY_JSON = URL_DICTIONARY + "/{0}.json";
	public static final String URL_DICTIONARY_ITEMS_JSON = URL_DICTIONARY + "/{0}/items.json";
	public static final String URL_DICTIONARY_ITEM_JSON = URL_DICTIONARY + "/item/{0}.json";

	/*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	public static final String URL_TAG = "/rest/tag";
	public static final String URL_TAGS_JSON = URL_TAG + "/search.json";
	public static final String URL_TAG_JSON = URL_TAG + "/{0}.json";
	public static final String URL_TAG_ISSUES_COUNT = URL_TAG + "/{0}/issues.count";

	/*---------------------------------*
     *             Filtres             *
     *---------------------------------*/
	public static final String URL_VIEW_FILTER = "/rest/viewFilter";
	public static final String URL_VIEW_FILTERS_JSON = URL_VIEW_FILTER + "/search.json";
	public static final String URL_VIEW_FILTER_JSON = URL_VIEW_FILTER + "/{0}.json";
	public static final String URL_VIEW_FILTER_ISSUES_JSON = URL_VIEW_FILTER + "/issues.json";
	public static final String URL_VIEW_FILTER_ISSUES_COUNT = URL_VIEW_FILTER + "/issues.count";
	
	/*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
	public static final String URL_CUSTOM_FIELD = "/rest/customField";
	public static final String URL_CUSTOM_FIELD_TYPES_JSON = URL_CUSTOM_FIELD + "/types.json";
	public static final String URL_CUSTOM_FIELDS_JSON = URL_CUSTOM_FIELD + "/search.json";
	public static final String URL_CUSTOM_FIELD_JSON = URL_CUSTOM_FIELD + "/{0}.json";
	public static final String URL_CUSTOM_VALUES_BY_FIELD_JSON = URL_CUSTOM_FIELD + "/{0}/customValues.json";
	
	/*---------------------------------*
     *             Sprints             *
     *---------------------------------*/
	public static final String URL_SPRINT = "/rest/sprint";
	public static final String URL_SPRINTS_JSON = "/rest/sprint/search.json";
	public static final String URL_SPRINT_JSON = URL_SPRINT + "/{0}.json";
	
	// Incidencies
	public static final String URL_ISSUE = "/rest/issue";
	public static final String URL_ISSUE_JSON = "/rest/issue/{0}.json";
	public static final String URL_ISSUE_SEARCH = "/rest/issue/search.json";
	public static final String URL_ISSUE_COUNT = "/rest/issue/search.count";
	public static final String URL_ISSUE_CUSTOM_VALUES_JSON = "rest/issue/{0}/customValues.json";
	
	/*---------------------------------*
     *          Issue Notes            *
     *---------------------------------*/
	public static final String URL_ISSUE_NOTE = "/rest/issue/{0}/note";
	public static final String URL_ISSUE_NOTES_JSON = "/rest/issue/{0}/notes.json";
	public static final String URL_ISSUE_NOTE_JSON = URL_ISSUE_NOTE + "/{1}.json";
	
	/*---------------------------------*
     *           Issue Tags            *
     *---------------------------------*/
	public static final String URL_ISSUE_TAG = "/rest/issue/{0}/tag";
	public static final String URL_ISSUE_TAGS_JSON = "/rest/issue/{0}/tags.json";
	
	/*---------------------------------*
     *       Registres de treball      *
     *---------------------------------*/
	public static final String URL_ISSUE_WORK_REGISTRY = "/rest/issue/{0}/workRegistry";
	public static final String URL_ISSUE_WORK_REGISTRIES_JSON = "/rest/issue/{0}/workRegistries.json";
	public static final String URL_ISSUE_WORK_REGISTRY_JSON = URL_ISSUE_WORK_REGISTRY + "/{1}.json";
	
	/*---------------------------------*
     *         Registres W.I.P.        *
     *---------------------------------*/
	public static final String URL_ISSUE_WIP_REGISTRY = "/rest/issue/{0}/wipRegistry";
	public static final String URL_ISSUE_WIP_REGISTRY_SEARCH_JSON = URL_ISSUE_WIP_REGISTRY + ".json";
	public static final String URL_ISSUE_WIP_REGISTRY_JSON = URL_ISSUE_WIP_REGISTRY + "/{1}.json";
	public static final String URL_USER_WIP_REGISTRIES_JSON = "/rest/user/{0}/wipRegistries.json";
	
	/*---------------------------------*
     *       Providers Projectes       *
     *---------------------------------*/
	public static final String URL_PROVIDER_PROJECTS_JSON = "/rest/provider/projects.json";
	public static final String URL_PROVIDER_SPECIFIC_PROJECTS_JSON = "/rest/provider/{0}/projects.json";
	public static final String URL_PROVIDER_PROJECT_JSON = "/rest/provider/project/{0}.json";
	
	/*---------------------------------*
     *         Providers Estats        *
     *---------------------------------*/
	public static final String URL_PROVIDER_STATUS_ALL_JSON = "/rest/provider/status.json";
	public static final String URL_PROVIDER_SPECIFIC_STATUS_JSON = "/rest/provider/{0}/status.json";
	public static final String URL_PROVIDER_STATUS_JSON = "/rest/provider/status/{0}.json";
	
	/*---------------------------------*
     *       Providers Severitats      *
     *---------------------------------*/
	public static final String URL_PROVIDER_SEVERITIES_JSON = "/rest/provider/severities.json";
	public static final String URL_PROVIDER_SPECIFIC_SEVERITIES_JSON = "/rest/provider/{0}/severities.json";
	public static final String URL_PROVIDER_SEVERITY_JSON = "/rest/provider/severity/{0}.json";
	
	/*---------------------------------*
     *       Providers Categories      *
     *---------------------------------*/
	public static final String URL_PROVIDER_CATEGORIES_JSON = "/rest/provider/categories.json";
	public static final String URL_PROVIDER_SPECIFIC_CATEGORIES_JSON = "/rest/provider/{0}/categories.json";
	public static final String URL_PROVIDER_CATEGORY_JSON = "/rest/provider/category/{0}.json";
	
	/*---------------------------------*
     *       		Usuaris            *
     *---------------------------------*/
	public static final String URL_USERS_JSON = "/rest/user/search.json";
	public static final String URL_USER_JSON = "/rest/user/{0}.json";
}
