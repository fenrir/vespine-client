package org.fenrir.vespine.client.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.builder.AlertTypeJsonBuilder;
import org.fenrir.vespine.client.builder.CategoryJsonBuilder;
import org.fenrir.vespine.client.builder.CustomFieldJsonBuilder;
import org.fenrir.vespine.client.builder.DictionaryItemJsonBuilder;
import org.fenrir.vespine.client.builder.DictionaryJsonBuilder;
import org.fenrir.vespine.client.builder.FieldTypeJsonBuilder;
import org.fenrir.vespine.client.builder.ProjectJsonBuilder;
import org.fenrir.vespine.client.builder.ProviderElementJsonBuilder;
import org.fenrir.vespine.client.builder.SeverityJsonBuilder;
import org.fenrir.vespine.client.builder.StatusJsonBuilder;
import org.fenrir.vespine.client.builder.TagJsonBuilder;
import org.fenrir.vespine.client.builder.ViewFilterJsonBuilder;
import org.fenrir.vespine.client.builder.WorkflowJsonBuilder;
import org.fenrir.vespine.client.dao.WebResourceJsonDAO;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class AdministrationFacadeRESTImpl implements IAdministrationFacade 
{
	private final Logger log = LoggerFactory.getLogger(AdministrationFacadeRESTImpl.class);
	
	@Inject
	private IUserCredentialsProvider credentialsProvider;
	
	@Inject
	private IClientBroadcastService broadcastService;
	
	private WebResourceJsonDAO resourceDAO;
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	public void setBroadcastService(IClientBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resourceDAO = new WebResourceJsonDAO(endpoint);
	}
	
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
	@Override
	public List<IProjectDTO> findAllProjects() throws BusinessException
	{
		String url = RESTConstants.URL_PROJECTS_JSON;
		JSONArray projects = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IProjectDTO> resultList = new ArrayList<IProjectDTO>();
		ProjectJsonBuilder builder = new ProjectJsonBuilder();
		for(int i=0; i<projects.length(); i++){
			try{
				JSONObject project = projects.getJSONObject(i);
				resultList.add(builder.setJsonObj(project).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<IProjectDTO> findProjectsByWorkflow(String workflowId) throws BusinessException
	{
		String url = RESTConstants.URL_PROJECTS_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("workflow", workflowId);
		
		JSONArray projects = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<IProjectDTO> resultList = new ArrayList<IProjectDTO>();
		ProjectJsonBuilder builder = new ProjectJsonBuilder();
		for(int i=0; i<projects.length(); i++){
			try{
				JSONObject project = projects.getJSONObject(i);
				resultList.add(builder.setJsonObj(project).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public IProjectDTO findProjectById(String id) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_PROJECT_JSON, id);
		JSONObject project = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		ProjectJsonBuilder builder = new ProjectJsonBuilder();
		try{
			return builder.setJsonObj(project).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IProjectDTO createProject(String name, String abbreviation, String workflow, List<String> providerProjects) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", name);
		queryParams.put("workflow", workflow);
		queryParams.put("abbreviation", abbreviation);
		if(providerProjects!=null){
			for(String elem:providerProjects){
				queryParams.put("providerProject", elem);
			}
		}
		
		String projectLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_PROJECT, 
				queryParams);
		String projectId = StringUtils.substringAfterLast(projectLocation, "/");
		projectId = StringUtils.substringBefore(projectId, ".json");
		return findProjectById(projectId);
	}
	
	@Override
	public IProjectDTO updateProject(String id, String name, String workflow, List<String> providerProjects) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", name);
		queryParams.put("workflow", workflow);
		if(providerProjects!=null){
			for(String elem:providerProjects){
				queryParams.put("providerProject", elem);
			}
		}
		
		String url = RESTConstants.URL_PROJECT + "/" + id;
		String projectLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(projectLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findProjectById(retrievedId);
	}
	
	@Override
	public void deleteProject(String id) throws BusinessException
	{
		String url = RESTConstants.URL_PROJECT + "/" + id;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *           Categories            *
     *---------------------------------*/
	@Override
    public List<ICategoryDTO> findAllCategories() throws BusinessException
    {
		String url = RESTConstants.URL_CATEGORIES_JSON;
		JSONArray categories = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ICategoryDTO> resultList = new ArrayList<ICategoryDTO>();
		CategoryJsonBuilder builder = new CategoryJsonBuilder();
		for(int i=0; i<categories.length(); i++){
			try{
				JSONObject category = categories.getJSONObject(i);
				resultList.add(builder.setJsonObj(category).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public ICategoryDTO findCategoryById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_CATEGORY_JSON, id);
		JSONObject category = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		CategoryJsonBuilder builder = new CategoryJsonBuilder();
		try{
			return builder.setJsonObj(category).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public ICategoryDTO createCategory(String name, List<String> providerCategories) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		if(providerCategories!=null){
			for(String elem:providerCategories){
				queryParams.put("providerCategory", elem);
			}
		}
		
		String categoryLocation = resourceDAO.create(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_CATEGORY, 
				queryParams);
		String categoryId = StringUtils.substringAfterLast(categoryLocation, "/");
		categoryId = StringUtils.substringBefore(categoryId, ".json");
		return findCategoryById(categoryId);
    }
    
    @Override
    public ICategoryDTO updateCategory(String id, String name, List<String> providerCategories) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		if(providerCategories!=null){
			for(String elem:providerCategories){
				queryParams.put("providerCategory", elem);
			}
		}
		
		String url = RESTConstants.URL_CATEGORY + "/" + id;
		String categoryLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(categoryLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findCategoryById(retrievedId);
    }
    
    @Override
    public void deleteCategory(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_CATEGORY + "/" + id;
    	resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    @Override
    public List<ISeverityDTO> findAllSeverities() throws BusinessException
    {
    	String url = RESTConstants.URL_SEVERITIES_JSON;
		JSONArray severities = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ISeverityDTO> resultList = new ArrayList<ISeverityDTO>();
		SeverityJsonBuilder builder = new SeverityJsonBuilder();
		for(int i=0; i<severities.length(); i++){
			try{
				JSONObject severity = severities.getJSONObject(i);
				resultList.add(builder.setJsonObj(severity).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public ISeverityDTO findSeverityById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_SEVERITY_JSON, id);
		JSONObject severity = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		SeverityJsonBuilder builder = new SeverityJsonBuilder();
		try{
			return builder.setJsonObj(severity).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public ISeverityDTO createSeverity(String name, String slaPattern, List<String> providerSeverities) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("slaPattern", slaPattern);
		if(providerSeverities!=null){
			for(String elem:providerSeverities){
				queryParams.put("providerSeverity", elem);
			}
		}
		
		String severityLocation = resourceDAO.create(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_SEVERITY, 
				queryParams);
		String severityId = StringUtils.substringAfterLast(severityLocation, "/");
		severityId = StringUtils.substringBefore(severityId, ".json");
		return findSeverityById(severityId);
    }
    
    @Override
    public ISeverityDTO updateSeverity(String id, String name, String slaPattern, List<String> providerSeverities) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("slaPattern", slaPattern);
		if(providerSeverities!=null){
			for(String elem:providerSeverities){
				queryParams.put("providerSeverity", elem);
			}
		}
		
		String url = RESTConstants.URL_SEVERITY + "/" + id;
		String severityLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(severityLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findSeverityById(retrievedId);
    }
    
    @Override
    public void deleteSeverity(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_SEVERITY + "/" + id;
    	resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
    @Override
    public List<IStatusDTO> findAllStatus() throws BusinessException
    {
    	String url = RESTConstants.URL_STATUS_ALL_JSON;
		JSONArray statusList = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IStatusDTO> resultList = new ArrayList<IStatusDTO>();
		StatusJsonBuilder builder = new StatusJsonBuilder();
		for(int i=0; i<statusList.length(); i++){
			try{
				JSONObject status = statusList.getJSONObject(i);
				resultList.add(builder.setJsonObj(status).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public IStatusDTO findStatusById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_STATUS_JSON, id);
		JSONObject status = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		StatusJsonBuilder builder = new StatusJsonBuilder();
		try{
			return builder.setJsonObj(status).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public List<IStatusDTO> findWorkflowNextStatus(String projectId, String currentStatus) throws BusinessException
    {
    	String url = RESTConstants.URL_STATUS_NEXT_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("projectId", projectId);
		queryParams.put("currentStatus", currentStatus);
		
		JSONArray statusList = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<IStatusDTO> resultList = new ArrayList<IStatusDTO>();
		StatusJsonBuilder builder = new StatusJsonBuilder();
		for(int i=0; i<statusList.length(); i++){
			try{
				JSONObject status = statusList.getJSONObject(i);
				resultList.add(builder.setJsonObj(status).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public String getStatusColor(String statusId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_STATUS_COLOR, statusId);
		String color = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return color;
    }
    
    @Override
    public IStatusDTO createStatus(String name, String color, List<String> providerStatus) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("color", color);
		if(providerStatus!=null){
			for(String elem:providerStatus){
				queryParams.put("providerStatus", elem);
			}
		}
		
		String statusLocation = resourceDAO.create(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_STATUS, 
				queryParams);
		String statusId = StringUtils.substringAfterLast(statusLocation, "/");
		statusId = StringUtils.substringBefore(statusId, ".json");
		return findStatusById(statusId);
    }
    
    public IStatusDTO updateStatus(String id, String name, String color, List<String> providerStatus) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("color", color);
		if(providerStatus!=null){
			for(String elem:providerStatus){
				queryParams.put("providerStatus", elem);
			}
		}
		
		String url = RESTConstants.URL_STATUS + "/" + id;
		String statusLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(statusLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findStatusById(retrievedId);
    }
    
    @Override
    public void deleteIssueStatus(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_STATUS + "/" + id;
    	resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *           Workflow              *
     *---------------------------------*/
    @Override
    public List<IWorkflowDTO> findAllWorkflows() throws BusinessException
    {
    	String url = RESTConstants.URL_WORKFLOWS_JSON;
		JSONArray workflows = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IWorkflowDTO> resultList = new ArrayList<IWorkflowDTO>();
		WorkflowJsonBuilder builder = new WorkflowJsonBuilder();
		for(int i=0; i<workflows.length(); i++){
			try{
				JSONObject workflow = workflows.getJSONObject(i);
				resultList.add(builder.setJsonObj(workflow).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public IWorkflowDTO findWorkflowById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_WORKFLOW_JSON, id);
		JSONObject workflow = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		WorkflowJsonBuilder builder = new WorkflowJsonBuilder();
		try{
			return builder.setJsonObj(workflow).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public boolean hasWorkflowRelatedProjects(String workflowId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_WORKFLOW_PROJECTS_JSON, workflowId);
    	JSONArray workflows = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    	return workflows.length()>0;
    }
    
    @Override
    public IWorkflowDTO createWorkflow(String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException
    {
    	try{
	    	MultiValueMap queryParams = new MultiValueMap();
	    	queryParams.put("name", name);
			queryParams.put("initialStatus", initialStatus);
			if(steps!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(IWorkflowStepDTO step:steps){
					String strStep = mapper.writeValueAsString(step);
					queryParams.put("step", strStep);
				}
			}
			
			String workflowLocation = resourceDAO.create(broadcastService.getConnectionId(),
					credentialsProvider.getUsername(), 
					credentialsProvider.getPassword(), 
					RESTConstants.URL_WORKFLOW, 
					queryParams);
			String workflowId = StringUtils.substringAfterLast(workflowLocation, "/");
			workflowId = StringUtils.substringBefore(workflowId, ".json");
			return findWorkflowById(workflowId);
    	}
    	catch(Exception e){
    		log.error("Error durant la creació del fluxe de treball: {}", e.getMessage(), e);
    		throw new BusinessException("Error durant la creació del fluxe de treball: " + e.getMessage(), e);
    	}
    }
    
    @Override
	public IWorkflowDTO updateWorkflow(String id, String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException
	{
    	try{
	    	MultiValueMap queryParams = new MultiValueMap();
	    	queryParams.put("name", name);
			queryParams.put("initialStatus", initialStatus);
			if(steps!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(IWorkflowStepDTO step:steps){
					String strStep = mapper.writeValueAsString(step);
					queryParams.put("step", strStep);
				}
			}
			
			String url = RESTConstants.URL_WORKFLOW + "/" + id;
			String workflowLocation = resourceDAO.update(broadcastService.getConnectionId(),
					credentialsProvider.getUsername(), 
					credentialsProvider.getPassword(), 
					url, 
					queryParams);
			String retrievedId = StringUtils.substringAfterLast(workflowLocation, "/");
			retrievedId = StringUtils.substringBefore(retrievedId, ".json");
			return findWorkflowById(retrievedId);
    	}
    	catch(Exception e){
    		log.error("Error durant la modificació del fluxe de treball: {}", e.getMessage(), e);
    		throw new BusinessException("Error durant la modificació del fluxe de treball: " + e.getMessage(), e);
    	}
	}
	
    @Override
	public void deleteWorkflow(String id) throws BusinessException
	{
    	String url = RESTConstants.URL_WORKFLOW + "/" + id;
    	resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
    @Override
	public List<IAlertTypeDTO> findAllAlertTypes() throws BusinessException
	{
    	String url = RESTConstants.URL_ALERT_TYPES_JSON;
		JSONArray alertTypes = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IAlertTypeDTO> resultList = new ArrayList<IAlertTypeDTO>();
		AlertTypeJsonBuilder builder = new AlertTypeJsonBuilder();
		for(int i=0; i<alertTypes.length(); i++){
			try{
				JSONObject alertType = alertTypes.getJSONObject(i);
				resultList.add(builder.setJsonObj(alertType).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
    @Override
	public IAlertTypeDTO findAlertTypeById(String id) throws BusinessException
	{
    	String url = MessageFormat.format(RESTConstants.URL_ALERT_TYPE_JSON, id);
		JSONObject alertType = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		AlertTypeJsonBuilder builder = new AlertTypeJsonBuilder();
		try{
			return builder.setJsonObj(alertType).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
    @Override
	public IAlertTypeDTO createAlertType(String name, String description, String icon, String alertRule, boolean generateOld) throws BusinessException
	{
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("description", description);
		queryParams.put("icon", icon);
		queryParams.put("alertRule", alertRule);
		queryParams.put("generateOld", Boolean.toString(generateOld));
		
		String alertTypeLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_ALERT_TYPE, 
				queryParams);
		String alertTypeId = StringUtils.substringAfterLast(alertTypeLocation, "/");
		alertTypeId = StringUtils.substringBefore(alertTypeId, ".json");
		return findAlertTypeById(alertTypeId);
	}
	
    @Override
    public IAlertTypeDTO updateAlertType(String id, String name, String description, String icon, String alertRule) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("description", description);
		queryParams.put("icon", icon);
		queryParams.put("alertRule", alertRule);

		String url = RESTConstants.URL_ALERT_TYPE + "/" + id;
		String alertTypeLocation = resourceDAO.update(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String alertTypeId = StringUtils.substringAfterLast(alertTypeLocation, "/");
		alertTypeId = StringUtils.substringBefore(alertTypeId, ".json");
		return findAlertTypeById(alertTypeId);
    }
    
    @Override
    public void deleteAlertType(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_ALERT_TYPE + "/" + id;
    	resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    @Override
    public List<ITagDTO> findAllTags() throws BusinessException
    {
    	String url = RESTConstants.URL_TAGS_JSON;
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ITagDTO> findTagsFiltered(String prefix) throws BusinessException
    {
    	String url = RESTConstants.URL_TAGS_JSON;
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("nameFilter", prefix);
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ITagDTO> findTagsByName(String name) throws BusinessException
    {
    	String url = RESTConstants.URL_TAGS_JSON;
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", name);
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ITagDTO> findPreferredTags() throws BusinessException
    {
    	String url = RESTConstants.URL_TAGS_JSON;
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("preferred", Boolean.TRUE.toString());
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ITagDTO> findPreferredTagsFiltered(String prefix) throws BusinessException
    {
    	String url = RESTConstants.URL_TAGS_JSON;
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("preferred", Boolean.TRUE.toString());
		queryParams.put("nameFilter", prefix);
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public ITagDTO findTagById(String tagId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_TAG_JSON, tagId);
		JSONObject tag = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		TagJsonBuilder builder = new TagJsonBuilder();
		try{
			return builder.setJsonObj(tag).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public ITagDTO createTag(String name, boolean preferred) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("preferred", Boolean.toString(preferred));
		
		String tagLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_TAG, 
				queryParams);
		String tagId = StringUtils.substringAfterLast(tagLocation, "/");
		tagId = StringUtils.substringBefore(tagId, ".json");
		return findTagById(tagId);
    }
    
    @Override
    public ITagDTO updateTag(String tagId, String name, boolean preferred) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("name", name);
		queryParams.put("preferred", Boolean.toString(preferred));
		
		String url = RESTConstants.URL_TAG + "/" + tagId;
		String tagLocation = resourceDAO.update(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(tagLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findTagById(retrievedId);
    }
    
    @Override
    public void deleteTag(String tagId) throws BusinessException
    {
    	String url = RESTConstants.URL_TAG + "/" + tagId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
    @Override
    public List<IDictionaryDTO> findAllDictionaries() throws BusinessException
    {
    	String url = RESTConstants.URL_DICTIONARIES_JSON;
		JSONArray diactionaries = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IDictionaryDTO> resultList = new ArrayList<IDictionaryDTO>();
		DictionaryJsonBuilder builder = new DictionaryJsonBuilder();
		for(int i=0; i<diactionaries.length(); i++){
			try{
				JSONObject dictionary = diactionaries.getJSONObject(i);
				resultList.add(builder.setJsonObj(dictionary).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public IDictionaryDTO findDictionaryById(String dictionaryId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_DICTIONARY_JSON, dictionaryId);
		JSONObject dictionary = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		DictionaryJsonBuilder builder = new DictionaryJsonBuilder();
		try{
			return builder.setJsonObj(dictionary).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public List<IDictionaryItemDTO> findDictionaryItems(String dictionaryId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_DICTIONARY_ITEMS_JSON, dictionaryId);
    	JSONArray items = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IDictionaryItemDTO> resultList = new ArrayList<IDictionaryItemDTO>();
		DictionaryItemJsonBuilder builder = new DictionaryItemJsonBuilder();
		for(int i=0; i<items.length(); i++){
			try{
				JSONObject item = items.getJSONObject(i);
				resultList.add(builder.setJsonObj(item).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public IDictionaryItemDTO findDictionaryItem(String itemId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_DICTIONARY_ITEM_JSON, itemId);
		JSONObject item = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		DictionaryItemJsonBuilder builder = new DictionaryItemJsonBuilder();
		try{
			return builder.setJsonObj(item).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public IDictionaryDTO createDictionary(String name, String description, List<IDictionaryItemDTO> items) throws BusinessException
    {
    	try{
	    	MultiValueMap queryParams = new MultiValueMap();
			queryParams.put("name", name);
			queryParams.put("description", description);
			if(items!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(IDictionaryItemDTO item:items){
					String strItem = mapper.writeValueAsString(item);
					queryParams.put("item", strItem);
				}
			}
			
			String location = resourceDAO.create(broadcastService.getConnectionId(), 
					credentialsProvider.getUsername(), 
					credentialsProvider.getPassword(), 
					RESTConstants.URL_DICTIONARY, 
					queryParams);
			String dictionaryId = StringUtils.substringAfterLast(location, "/");
			dictionaryId = StringUtils.substringBefore(dictionaryId, ".json");
			return findDictionaryById(dictionaryId);
    	}
    	catch(Exception e){
    		log.error("Error durant la creació del diccionari: {}", e.getMessage(), e);
    		throw new BusinessException("Error durant creació del diccionari: " + e.getMessage(), e);
    	}
    }
    
    @Override
    public IDictionaryDTO updateDictionary(String id, String name, String description, List<IDictionaryItemDTO> items) throws BusinessException
    {
    	try{
	    	MultiValueMap queryParams = new MultiValueMap();
			queryParams.put("name", name);
			queryParams.put("description", description);
			if(items!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(IDictionaryItemDTO item:items){
					String strItem = mapper.writeValueAsString(item);
					queryParams.put("item", strItem);
				}
			}
			
			String url = RESTConstants.URL_DICTIONARY + "/" + id;
			String location = resourceDAO.update(broadcastService.getConnectionId(), 
					credentialsProvider.getUsername(), 
					credentialsProvider.getPassword(), 
					url, 
					queryParams);
			String dictionaryId = StringUtils.substringAfterLast(location, "/");
			dictionaryId = StringUtils.substringBefore(dictionaryId, ".json");
			return findDictionaryById(dictionaryId);
    	}
    	catch(Exception e){
    		log.error("Error durant la modificació del diccionari: {}", e.getMessage(), e);
    		throw new BusinessException("Error durant modificació del diccionari: " + e.getMessage(), e);
    	}
    }
    
    @Override
    public void deleteDictionary(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_DICTIONARY + "/" + id;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    @Override
    public List<IFieldType> getAllFieldTypes() throws BusinessException
    {
    	String url = RESTConstants.URL_CUSTOM_FIELD_TYPES_JSON;
		JSONArray fieldTypes = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IFieldType> resultList = new ArrayList<IFieldType>();
		FieldTypeJsonBuilder builder = new FieldTypeJsonBuilder();
		for(int i=0; i<fieldTypes.length(); i++){
			try{
				JSONObject type = fieldTypes.getJSONObject(i);
				resultList.add(builder.setJsonObj(type).build());
			}
			catch(Exception e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ICustomFieldDTO> findAllCustomFields() throws BusinessException
    {
    	String url = RESTConstants.URL_CUSTOM_FIELDS_JSON;
		JSONArray customFields = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ICustomFieldDTO> resultList = new ArrayList<ICustomFieldDTO>();
		CustomFieldJsonBuilder builder = new CustomFieldJsonBuilder();
		for(int i=0; i<customFields.length(); i++){
			try{
				JSONObject customField = customFields.getJSONObject(i);
				resultList.add(builder.setJsonObj(customField).build());
			}
			catch(Exception e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ICustomFieldDTO> findCustomFieldsByProject(String projectId) throws BusinessException
    {
    	String url = RESTConstants.URL_CUSTOM_FIELDS_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("projectId", projectId);
		
		JSONArray customFields = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<ICustomFieldDTO> resultList = new ArrayList<ICustomFieldDTO>();
		CustomFieldJsonBuilder builder = new CustomFieldJsonBuilder();
		for(int i=0; i<customFields.length(); i++){
			try{
				JSONObject customField = customFields.getJSONObject(i);
				resultList.add(builder.setJsonObj(customField).build());
			}
			catch(Exception e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public ICustomFieldDTO findCustomFieldById(String fieldId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_CUSTOM_FIELD_JSON, fieldId);
		JSONObject field = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		CustomFieldJsonBuilder builder = new CustomFieldJsonBuilder();
		try{
			return builder.setJsonObj(field).build();
		}
		catch(Exception e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public ICustomFieldDTO createCustomField(String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", name);
		queryParams.put("fieldType", fieldType.getType());
		queryParams.put("dataProvider", dataProviderId);
		queryParams.put("mandatory", Boolean.toString(mandatory));
		if(projects!=null){
			for(String project:projects){
				queryParams.put("project", project);
			}
		}
		
		String location = resourceDAO.create(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_CUSTOM_FIELD, 
				queryParams);
		String fieldId = StringUtils.substringAfterLast(location, "/");
		fieldId = StringUtils.substringBefore(fieldId, ".json");
		return findCustomFieldById(fieldId);
    }
    
    @Override
    public ICustomFieldDTO updateCustomField(String id, String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", name);
		queryParams.put("fieldType", fieldType.getType());
		queryParams.put("dataProvider", dataProviderId);
		queryParams.put("mandatory", Boolean.toString(mandatory));
		if(projects!=null){
			for(String project:projects){
				queryParams.put("project", project);
			}
		}
		
		String url = RESTConstants.URL_CUSTOM_FIELD + "/" + id;
		String location = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String fieldId = StringUtils.substringAfterLast(location, "/");
		fieldId = StringUtils.substringBefore(fieldId, ".json");
		return findCustomFieldById(fieldId);
    }
    
    @Override
    public void deleteCustomField(String id) throws BusinessException
    {
    	String url = RESTConstants.URL_CUSTOM_FIELD + "/" + id;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
    
    /*---------------------------------*
     *       Providers Projectes       *
     *---------------------------------*/
    @Override
    public List<IProviderElementDTO> findAllProviderProjects() throws BusinessException
    {
		return findProviderElements(RESTConstants.URL_PROVIDER_PROJECTS_JSON, null);
    }
    
    @Override
    public List<IProviderElementDTO> findProviderProjectsByProvider(String provider) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("provider", provider);
		return findProviderElements(RESTConstants.URL_PROVIDER_PROJECTS_JSON, queryParams);
    }
    
    @Override
    public IProviderElementDTO findProviderProjectById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_PROJECT_JSON, id);
    	return findSingleProviderElement(url, null);
    }
    
    @Override
    public IProviderElementDTO findProviderProjectByProviderData(String provider, String providerId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SPECIFIC_PROJECTS_JSON, provider);
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("providerId", providerId);
		List<IProviderElementDTO> elements = findProviderElements(url, queryParams);
		if(elements.isEmpty()){
			return null;
		}
		else{
			return elements.get(0);
		}
    }

    /*---------------------------------*
     *         Providers Estats        *
     *---------------------------------*/
    @Override
    public List<IProviderElementDTO> findAllProviderStatus() throws BusinessException
    {
    	return findProviderElements(RESTConstants.URL_PROVIDER_STATUS_ALL_JSON, null);
    }
    
    @Override
    public List<IProviderElementDTO> findProviderStatusByProvider(String provider) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SPECIFIC_STATUS_JSON, provider);
		return findProviderElements(url, null);
    }
    
    @Override
    public IProviderElementDTO findProviderStatusById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_STATUS_JSON, id);
    	return findSingleProviderElement(url, null);
    }
    
    @Override
    public IProviderElementDTO findProviderStatusByProviderData(String provider, String providerId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SPECIFIC_STATUS_JSON, provider);
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("providerId", providerId);
		List<IProviderElementDTO> elements = findProviderElements(url, queryParams);
		if(elements.isEmpty()){
			return null;
		}
		else{
			return elements.get(0);
		}
    }
    
    /*---------------------------------*
     *       Providers Severitats      *
     *---------------------------------*/
    @Override
    public List<IProviderElementDTO> findAllProviderSeverities() throws BusinessException
    {
    	return findProviderElements(RESTConstants.URL_PROVIDER_SEVERITIES_JSON, null);
    }
    
    @Override
    public List<IProviderElementDTO> findProviderSeveritiesByProvider(String provider) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("provider", provider);
		return findProviderElements(RESTConstants.URL_PROVIDER_SEVERITIES_JSON, queryParams);
    }
    
    @Override
    public IProviderElementDTO findProviderSeverityById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SEVERITY_JSON, id);
    	return findSingleProviderElement(url, null);
    }
    
    @Override
    public IProviderElementDTO findProviderSeverityByProviderData(String provider, String providerId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SPECIFIC_SEVERITIES_JSON, provider);
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("providerId", providerId);
		List<IProviderElementDTO> elements = findProviderElements(url, queryParams);
		if(elements.isEmpty()){
			return null;
		}
		else{
			return elements.get(0);
		}
    }
    
    /*---------------------------------*
     *       Providers Categories      *
     *---------------------------------*/
    @Override
    public List<IProviderElementDTO> findAllProviderCategories() throws BusinessException
    {
    	return findProviderElements(RESTConstants.URL_PROVIDER_CATEGORIES_JSON, null);
    }
    
    @Override
    public List<IProviderElementDTO> findProviderCategoriesByProvider(String provider) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("provider", provider);
		return findProviderElements(RESTConstants.URL_PROVIDER_CATEGORIES_JSON, queryParams);
    }
    
    @Override
    public IProviderElementDTO findProviderCategoryById(String id) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_CATEGORY_JSON, id);
    	return findSingleProviderElement(url, null);
    }
    
    @Override
    public IProviderElementDTO findProviderCategoryByProviderData(String provider, String providerId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_PROVIDER_SPECIFIC_CATEGORIES_JSON, provider);
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("providerId", providerId);
		List<IProviderElementDTO> elements = findProviderElements(url, queryParams);
		if(elements.isEmpty()){
			return null;
		}
		else{
			return elements.get(0);
		}
    }
    
    /*---------------------------------*
     *     Providers mètodes comuns    *
     *---------------------------------*/
    private List<IProviderElementDTO> findProviderElements(String url, MultiValueMap params) throws BusinessException
    {
    	JSONArray providerElements = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, params);
		
		List<IProviderElementDTO> resultList = new ArrayList<IProviderElementDTO>();
		ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
		for(int i=0; i<providerElements.length(); i++){
			try{
				JSONObject element = providerElements.getJSONObject(i);
				resultList.add(builder.setJsonObj(element).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    private IProviderElementDTO findSingleProviderElement(String url, MultiValueMap queryParams) throws BusinessException
    {
		JSONObject element = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		ProviderElementJsonBuilder builder = new ProviderElementJsonBuilder();
		try{
			return builder.setJsonObj(element).build();
	    }
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    /*---------------------------------*
     *             Filtres             *
     *---------------------------------*/
    @Override
    public List<IViewFilterDTO> findApplicationViewFilters() throws BusinessException
    {
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("applicationOnly", Boolean.TRUE.toString());
		
		JSONArray filters = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_VIEW_FILTERS_JSON, queryParams);
		
		List<IViewFilterDTO> resultList = new ArrayList<IViewFilterDTO>();
		ViewFilterJsonBuilder builder = new ViewFilterJsonBuilder();
		for(int i=0; i<filters.length(); i++){
			try{
				JSONObject filter = filters.getJSONObject(i);
				resultList.add(builder.setJsonObj(filter).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    @Override
    public List<IViewFilterDTO> findUserViewFilters() throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("user", credentialsProvider.getUsername());
		
		JSONArray filters = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_VIEW_FILTERS_JSON, queryParams);
		
		List<IViewFilterDTO> resultList = new ArrayList<IViewFilterDTO>();
		ViewFilterJsonBuilder builder = new ViewFilterJsonBuilder();
		for(int i=0; i<filters.length(); i++){
			try{
				JSONObject filter = filters.getJSONObject(i);
				resultList.add(builder.setJsonObj(filter).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public IViewFilterDTO findViewFilterById(String filterId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_VIEW_FILTER_JSON, filterId);
    	JSONObject element = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		ViewFilterJsonBuilder builder = new ViewFilterJsonBuilder();
		try{
			return builder.setJsonObj(element).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
    }
    
    @Override
    public void reorderViewFilter(String filterId, Integer order) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("order", Integer.toString(order));
		
		String url = RESTConstants.URL_VIEW_FILTER + "/" + filterId;
		resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
    }
    
    @Override
    public IViewFilterDTO createViewFilter(String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", filterName);
		queryParams.put("description", filterDescription);
		queryParams.put("query", filterQuery);
		queryParams.put("orderByClause", filterOrderClause);
		
		String location = resourceDAO.create(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_VIEW_FILTER, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(location, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findViewFilterById(retrievedId);
    }
    
    @Override
    public IViewFilterDTO updateViewFilter(String filterId, String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException
    {
    	MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("name", filterName);
		queryParams.put("description", filterDescription);
		queryParams.put("query", filterQuery);
		queryParams.put("orderClause", filterOrderClause);
		
		String url = RESTConstants.URL_VIEW_FILTER + "/" + filterId;
		String location = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(location, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findViewFilterById(retrievedId);
    }
    
    @Override
    public void deleteViewFilter(String filterId) throws BusinessException
    {
    	String url = RESTConstants.URL_VIEW_FILTER + "/" + filterId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }
}
