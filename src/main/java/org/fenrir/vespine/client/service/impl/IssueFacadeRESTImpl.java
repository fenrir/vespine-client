package org.fenrir.vespine.client.service.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.helper.IssueOperationHelper;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.builder.CustomValueJsonBuilder;
import org.fenrir.vespine.client.builder.IssueAlertJsonBuilder;
import org.fenrir.vespine.client.builder.IssueJsonBuilder;
import org.fenrir.vespine.client.builder.IssueNoteJsonBuilder;
import org.fenrir.vespine.client.builder.TagJsonBuilder;
import org.fenrir.vespine.client.dao.WebResourceJsonDAO;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class IssueFacadeRESTImpl implements IIssueFacade
{
	private static final String FILTER_VARIABLE_CURRENT_USER = "\\$\\{CURRENT_USER\\}";
	
	@Inject
	private IUserCredentialsProvider credentialsProvider;
	
	@Inject
	private IAdministrationFacade administrationFacade;
	
	@Inject
	private IClientBroadcastService broadcastService;
	
	private WebResourceJsonDAO resourceDAO;
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	public void setAdministrationFacade(IAdministrationFacade administrationFacade)
	{
		this.administrationFacade = administrationFacade;
	}
	
	public void setBroadcastService(IClientBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resourceDAO = new WebResourceJsonDAO(endpoint);
	}

	/*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
	@Override
	public List<IIssueDTO> findAllIssues(int page, int pageSize) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("page", Integer.toString(page));
		queryParams.put("pageSize", Integer.toString(pageSize));
		
		JSONArray issues = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_ISSUE_SEARCH, queryParams);
		
		List<IIssueDTO> resultList = new ArrayList<IIssueDTO>();
		IssueJsonBuilder builder = new IssueJsonBuilder();
		for(int i=0; i<issues.length(); i++){
			try{
				JSONObject issue = issues.getJSONObject(i);
				resultList.add(builder.setJsonObj(issue).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<IIssueDTO> findTaggedIssues(String tagId, int page, int pageSize) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("tag", tagId);
		queryParams.put("page", Integer.toString(page));
		queryParams.put("pageSize", Integer.toString(pageSize));
		
		JSONArray issues = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_ISSUE_SEARCH);
		
		List<IIssueDTO> resultList = new ArrayList<IIssueDTO>();
		IssueJsonBuilder builder = new IssueJsonBuilder();
		for(int i=0; i<issues.length(); i++){
			try{
				JSONObject issue = issues.getJSONObject(i);
				resultList.add(builder.setJsonObj(issue).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public long countTaggedIssues(String tagId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_TAG_ISSUES_COUNT, tagId);
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return Long.valueOf(count);
	}
	
	@Override
	public Collection<IIssueDTO> findIssueSearchHits(SearchQuery query, int page, int pageSize) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("query", query.getIndexQuery());
    	queryParams.put("page", Integer.toString(page));
    	queryParams.put("pageSize", Integer.toString(pageSize));
    	JSONArray issues = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_ISSUE_SEARCH, queryParams);
		
		List<IIssueDTO> resultList = new ArrayList<IIssueDTO>();
		IssueJsonBuilder builder = new IssueJsonBuilder();
		for(int i=0; i<issues.length(); i++){
			try{
				JSONObject issue = issues.getJSONObject(i);
				resultList.add(builder.setJsonObj(issue).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	/**
	 * TODO Revisar si la query s'ha de passar per POST
	 */
	@Override
	public List<IIssueDTO> findFilteredIssues(IViewFilterDTO filter, int page, int pageSize) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
    	String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
    	queryParams.put("query", filterQuery);
    	queryParams.put("orderByClause", filter.getOrderClause());
    	queryParams.put("page", Integer.toString(page));
    	queryParams.put("pageSize", Integer.toString(pageSize));
    	JSONArray issues = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_VIEW_FILTER_ISSUES_JSON, queryParams);
		
		List<IIssueDTO> resultList = new ArrayList<IIssueDTO>();
		IssueJsonBuilder builder = new IssueJsonBuilder();
		for(int i=0; i<issues.length(); i++){
			try{
				JSONObject issue = issues.getJSONObject(i);
				resultList.add(builder.setJsonObj(issue).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public SearchQuery buildIssueTermSearchQuery(String term) throws BusinessException
	{
		return IssueOperationHelper.buildSearchQuery(term);
	}
	
	@Override
    public long countIssueSearchHits(SearchQuery query) throws BusinessException
    {
		MultiValueMap queryParams = new MultiValueMap();
    	queryParams.put("query", query.getIndexQuery());
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_ISSUE_COUNT, queryParams);
		
		return Long.valueOf(count);
    }
    
    @Override
	public long countIssuesByProject(String projectId) throws BusinessException
	{
    	String url = MessageFormat.format(RESTConstants.URL_PROJECT_ISSUES_COUNT, projectId);
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return Long.valueOf(count);
	}
    
    @Override
	public long countIssuesByCategory(String categoryId) throws BusinessException
	{
    	String url = MessageFormat.format(RESTConstants.URL_CATEGORY_ISSUES_COUNT, categoryId);
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return Long.valueOf(count);
	}
    
    @Override
	public long countIssuesBySeverity(String severityId) throws BusinessException
	{
    	String url = MessageFormat.format(RESTConstants.URL_SEVERITY_ISSUES_COUNT, severityId);
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return Long.valueOf(count);
	}
    
    @Override
	public long countIssuesByStatus(String statusId) throws BusinessException
	{
    	String url = MessageFormat.format(RESTConstants.URL_STATUS_ISSUES_COUNT, statusId);
		String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		return Long.valueOf(count);
	}
    
    @Override
	public long countFilteredIssues(IViewFilterDTO filter) throws BusinessException
	{
    	MultiValueMap queryParams = new MultiValueMap();
    	String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
    	queryParams.put("query", filterQuery);
    	String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_VIEW_FILTER_ISSUES_COUNT, queryParams);
    	
    	return Long.valueOf(count);
	}
	
	@Override
	public IIssueDTO findIssueById(String id) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_JSON, id);
		JSONObject issue = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		IssueJsonBuilder builder = new IssueJsonBuilder();
		try{
			return builder.setJsonObj(issue).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IIssueDTO findIssueByVisibleId(String visibleId) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("visibleId", visibleId);
		
		JSONArray issues = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_ISSUE_SEARCH, queryParams);
		IssueJsonBuilder builder = new IssueJsonBuilder();
		// Només n'hi haria d'haver una...
		IIssueDTO issueDTO = null;
		for(int i=0; i<issues.length(); i++){
			try{
				JSONObject issue = issues.getJSONObject(i);
				issueDTO = builder.setJsonObj(issue).build();
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}

		return issueDTO;
	}
	
	@Override
	public boolean issueFilterMatches(IViewFilterDTO filter, String issueId) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
    	String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
    	queryParams.put("query", filterQuery);
    	queryParams.put("issue", issueId);
    	String url = MessageFormat.format(RESTConstants.URL_VIEW_FILTER_ISSUES_COUNT, filter.getFilterId());
    	String count = resourceDAO.getStringValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
    	
    	return !"0".equals(count); 
	}
	
	@Override
	public IIssueDTO createIssue(String projectId,
			String categoryId,
			String severityId, 
			String statusId,
    		String summary, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Map<String, String> customFields) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("project", projectId);
		queryParams.put("category", categoryId);
		queryParams.put("severity", severityId);
		queryParams.put("status", statusId);
		queryParams.put("summary", summary);
		queryParams.put("description", description);
		queryParams.put("assignedUsername", assignedUser);
		if(sprintId!=null){
			queryParams.put("sprint", sprintId);
		}
		if(estimatedTime!=null){
			queryParams.put("estimatedTime", estimatedTime.toString());
		}
		if(slaDate!=null){
			queryParams.put("slaDate", new SimpleDateFormat("dd/MM/yyyy").format(slaDate));
		}
		if(customFields!=null){
			for(String key:customFields.keySet()){
				String value = customFields.get(key);
				if(value!=null){
					queryParams.put("customField", key+":"+value);
				}
			}
		}
		
		String issueLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_ISSUE, 
				queryParams);
		String issueId = StringUtils.substringAfterLast(issueLocation, "/");
		issueId = StringUtils.substringBefore(issueId, ".json");
		return findIssueById(issueId);
	}
	
	@Override
	public IIssueDTO updateIssue(String issueId, 
			String projectId,
			String categoryId,
			String severityId, 
			String statusId,
    		String summary, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Date resolutionDate,
    		Map<String, String> customFields) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("project", projectId);
		queryParams.put("category", categoryId);
		queryParams.put("severity", severityId);
		queryParams.put("status", statusId);
		queryParams.put("summary", summary);
		queryParams.put("description", description);
		queryParams.put("assignedUsername", assignedUser);
		if(sprintId!=null){
			queryParams.put("sprint", sprintId);
		}
		if(estimatedTime!=null){
			queryParams.put("estimatedTime", estimatedTime.toString());
		}
		if(slaDate!=null){
			queryParams.put("slaDate", new SimpleDateFormat("dd/MM/yyyy").format(slaDate));
		}
		if(resolutionDate!=null){
			queryParams.put("resolutionDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(resolutionDate));
		}
		if(customFields!=null){
			for(String key:customFields.keySet()){
				String value = customFields.get(key);
				queryParams.put("customField", key+":"+value);
			}
		}
		
		String url = RESTConstants.URL_ISSUE + "/" + issueId;
		String issueLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(issueLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueById(issueId);
	}
	
	@Override
	public void deleteIssue(String id, boolean bin) throws BusinessException
	{
		String url = RESTConstants.URL_ISSUE + "/" + id;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	public Date computeSLADate(Date sendDate, String severityId) throws BusinessException
	{
		ISeverityDTO severity = administrationFacade.findSeverityById(severityId);
		// El patró complirà l'expressió \\d+[mwdh]
        String slaPattern = severity.getSlaTerm();
        return IssueOperationHelper.computeSLADate(sendDate, slaPattern);
	}
	
	@Override
	public void updateIssueSLADate(String issueId, Date slaDate) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		if(slaDate!=null){
			queryParams.put("slaDate", new SimpleDateFormat("dd/MM/yyyy").format(slaDate));
		}
		queryParams.put("isSla", Boolean.TRUE.toString());
		
		String url = RESTConstants.URL_ISSUE + "/" + issueId + "/" + "sla";
		resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
	}
	
	@Override
	public void updateIssueSLADate(String issueId, boolean isSla) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("isSla", Boolean.toString(isSla));
		
		String url = RESTConstants.URL_ISSUE + "/" + issueId + "/" + "sla";
		resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
	}
	
	@Override
	public void restoreIssue(String issueId) throws BusinessException
	{
		String url = RESTConstants.URL_ISSUE + "/" + issueId + "/restore";
		resourceDAO.update(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, null);
	}
	
	/*---------------------------------*
     *              Notes              *
     *---------------------------------*/
	@Override
	public List<IIssueNoteDTO> findIssueNotes(String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_NOTES_JSON, issueId);
		JSONArray notes = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IIssueNoteDTO> resultList = new ArrayList<IIssueNoteDTO>();
		IssueNoteJsonBuilder builder = new IssueNoteJsonBuilder();
		for(int i=0; i<notes.length(); i++){
			try{
				JSONObject note = notes.getJSONObject(i);
				resultList.add(builder.setJsonObj(note).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public IIssueNoteDTO findIssueNote(String noteId, String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_NOTE_JSON, issueId, noteId);
		JSONObject note = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		IssueNoteJsonBuilder builder = new IssueNoteJsonBuilder();
		try{
			return builder.setJsonObj(note).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IIssueNoteDTO createIssueNote(String issueId, String noteContents) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("noteContents", noteContents);
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_NOTE, issueId);
		String noteLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(noteLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueNote(retrievedId, issueId);
	}
	
	@Override
	public IIssueNoteDTO updateIssueNote(String noteId, String issueId, String noteContents) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("noteContents", noteContents);
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_NOTE, issueId) + "/" + noteId;
		String noteLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(noteLocation, "/");
		issueId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueNote(issueId, retrievedId);
	}
	
	@Override
	public void deleteIssueNote(String issueId, String noteId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_NOTE, issueId) + "/" + noteId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	@Override
	public List<ITagDTO> findIssueTags(String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_TAGS_JSON, issueId);
		JSONArray tags = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		TagJsonBuilder builder = new TagJsonBuilder();
		for(int i=0; i<tags.length(); i++){
			try{
				JSONObject tag = tags.getJSONObject(i);
				resultList.add(builder.setJsonObj(tag).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public boolean isIssueTagged(String issueId, String tagId) throws BusinessException
	{
		List<ITagDTO> issueTags = findIssueTags(issueId);
		Iterator<ITagDTO> iterator = issueTags.iterator();
		while(iterator.hasNext()){
			ITagDTO tagDTO = iterator.next();
			if(tagId.equals(tagDTO.getTagId())){
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public void tagIssue(String issueId, String tagId) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("tagId", tagId);
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_TAG, issueId);
		resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
	}
	
	@Override
	public void tagIssueByDescription(String issueId, String tagDescription) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("tagDescription", tagDescription);
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_TAG, issueId);
		resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
	}
	
	@Override
	public void dettachIssueTag(String issueId, String tagId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_TAG, issueId) + "/" + tagId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
	@Override
    public List<ICustomValueDTO> findCustomValuesByField(String fieldId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_CUSTOM_VALUES_BY_FIELD_JSON, fieldId);
		JSONArray alerts = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ICustomValueDTO> resultList = new ArrayList<ICustomValueDTO>();
		CustomValueJsonBuilder builder = new CustomValueJsonBuilder();
		for(int i=0; i<alerts.length(); i++){
			try{
				JSONObject alert = alerts.getJSONObject(i);
				resultList.add(builder.setJsonObj(alert).build());
			}
			catch(Exception e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public List<ICustomValueDTO> findIssueCustomValues(String issueId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ISSUE_CUSTOM_VALUES_JSON, issueId);
		JSONArray fields = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ICustomValueDTO> resultList = new ArrayList<ICustomValueDTO>();
		CustomValueJsonBuilder builder = new CustomValueJsonBuilder();
		for(int i=0; i<fields.length(); i++){
			try{
				JSONObject field = fields.getJSONObject(i);
				resultList.add(builder.setJsonObj(field).build());
			}
			catch(Exception e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    @Override
    public List<IIssueAlertDTO> findAllAlertsByType(String alertTypeId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ALERTS_BY_TYPE_JSON, alertTypeId);
		JSONArray alerts = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IIssueAlertDTO> resultList = new ArrayList<IIssueAlertDTO>();
		IssueAlertJsonBuilder builder = new IssueAlertJsonBuilder();
		for(int i=0; i<alerts.length(); i++){
			try{
				JSONObject alert = alerts.getJSONObject(i);
				resultList.add(builder.setJsonObj(alert).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }

    @Override
    public List<IIssueAlertDTO> findIssueAlertsByIssueId(String issueId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ISSUE_ALERTS_JSON, issueId);
		JSONArray alerts = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IIssueAlertDTO> resultList = new ArrayList<IIssueAlertDTO>();
		IssueAlertJsonBuilder builder = new IssueAlertJsonBuilder();
		for(int i=0; i<alerts.length(); i++){
			try{
				JSONObject alert = alerts.getJSONObject(i);
				resultList.add(builder.setJsonObj(alert).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
    }
    
    @Override
    public void createIssueAlerts(String alertTypeId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ALERTS_BY_TYPE, alertTypeId);
		resourceDAO.create(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, null);
    }
    
    @Override
    public void validateIssueAlerts(String alertTypeId)throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ALERTS_BY_TYPE, alertTypeId);
		resourceDAO.update(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, null);
    }
    
    @Override
    public void deleteIssueAlert(String alertId, String issueId) throws BusinessException
    {
    	String url = MessageFormat.format(RESTConstants.URL_ISSUE_ALERT, issueId) + "/" + alertId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
    }

    /**
     * Estandaritzar per la part client i standalone
     * @param filterQuery
     * @return
     */
    private String replaceFilterQueryVariables(String filterQuery)
    {
    	String currentUser = credentialsProvider.getUsername();
    	filterQuery = filterQuery.replaceAll(FILTER_VARIABLE_CURRENT_USER, currentUser);
    	
    	return filterQuery;
    }
}
