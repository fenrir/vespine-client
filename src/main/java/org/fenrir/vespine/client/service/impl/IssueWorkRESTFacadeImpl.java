package org.fenrir.vespine.client.service.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.service.IIssueWorkFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.builder.SprintJsonBuilder;
import org.fenrir.vespine.client.builder.IssueWipRegistryJsonBuilder;
import org.fenrir.vespine.client.builder.WorkRegistryJsonBuilder;
import org.fenrir.vespine.client.dao.WebResourceJsonDAO;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140828
 */
public class IssueWorkRESTFacadeImpl implements IIssueWorkFacade 
{
	@Inject
	private IUserCredentialsProvider credentialsProvider;
	
	@Inject
	private IClientBroadcastService broadcastService;

	private WebResourceJsonDAO resourceDAO;
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	public void setBroadcastService(IClientBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resourceDAO = new WebResourceJsonDAO(endpoint);
	}
	
	/*---------------------------------*
     *             Sprints             *
     *---------------------------------*/
	@Override
	public List<ISprintDTO> findAllSprints() throws BusinessException
	{ 
		String url = RESTConstants.URL_SPRINTS_JSON;
		JSONArray sprints = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<ISprintDTO> resultList = new ArrayList<ISprintDTO>();
		SprintJsonBuilder builder = new SprintJsonBuilder();
		for(int i=0; i<sprints.length(); i++){
			try{
				JSONObject sprint = sprints.getJSONObject(i);
				resultList.add(builder.setJsonObj(sprint).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<ISprintDTO> findSprintsByProjectNameLike(String name) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("projectName", name);
		JSONArray sprints = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_SPRINTS_JSON);
		
		List<ISprintDTO> resultList = new ArrayList<ISprintDTO>();
		SprintJsonBuilder builder = new SprintJsonBuilder();
		for(int i=0; i<sprints.length(); i++){
			try{
				JSONObject sprint = sprints.getJSONObject(i);
				resultList.add(builder.setJsonObj(sprint).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<ISprintDTO> findSprintsByStartDateLike(String startDatePart) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("startDatePart", startDatePart);
		JSONArray sprints = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_SPRINTS_JSON);
		
		List<ISprintDTO> resultList = new ArrayList<ISprintDTO>();
		SprintJsonBuilder builder = new SprintJsonBuilder();
		for(int i=0; i<sprints.length(); i++){
			try{
				JSONObject sprint = sprints.getJSONObject(i);
				resultList.add(builder.setJsonObj(sprint).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<ISprintDTO> findSprintsByEndDateLike(String endDatePart) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("endDatePart", endDatePart);
		JSONArray sprints = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), RESTConstants.URL_SPRINTS_JSON);
		
		List<ISprintDTO> resultList = new ArrayList<ISprintDTO>();
		SprintJsonBuilder builder = new SprintJsonBuilder();
		for(int i=0; i<sprints.length(); i++){
			try{
				JSONObject sprint = sprints.getJSONObject(i);
				resultList.add(builder.setJsonObj(sprint).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public ISprintDTO findSprintById(String sprintId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_SPRINT_JSON, sprintId);
		JSONObject sprint = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		SprintJsonBuilder builder = new SprintJsonBuilder();
		try{
			return builder.setJsonObj(sprint).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public ISprintDTO createSprint(String projectId, Date startDate, Date endDate) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("project", projectId);
		queryParams.put("startDate", new SimpleDateFormat("dd/MM/yyyy").format(startDate));
		queryParams.put("endDate", new SimpleDateFormat("dd/MM/yyyy").format(endDate));
		
		String projectLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				RESTConstants.URL_SPRINT, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(projectLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findSprintById(retrievedId);
	}
	
	@Override
	public ISprintDTO updateSprint(String sprintId, String projectId, Date startDate, Date endDate) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("project", projectId);
		queryParams.put("startDate", new SimpleDateFormat("dd/MM/yyyy").format(startDate));
		queryParams.put("endDate", new SimpleDateFormat("dd/MM/yyyy").format(endDate));
		
		String url = RESTConstants.URL_SPRINT + "/" + sprintId;
		String projectLocation = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(projectLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findSprintById(retrievedId);
	}
	
	@Override
	public void deleteSprint(String sprintId) throws BusinessException
	{
		String url = RESTConstants.URL_SPRINT + "/" + sprintId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *      Registres de treball       *
     *---------------------------------*/
	@Override
	public List<IWorkRegistryDTO> findAllIssueWorkRegistries(String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WORK_REGISTRIES_JSON, issueId);
		JSONArray workRegistries = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IWorkRegistryDTO> resultList = new ArrayList<IWorkRegistryDTO>();
		WorkRegistryJsonBuilder builder = new WorkRegistryJsonBuilder();
		for(int i=0; i<workRegistries.length(); i++){
			try{
				JSONObject registry = workRegistries.getJSONObject(i);
				resultList.add(builder.setJsonObj(registry).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public IWorkRegistryDTO findIssueWorkRegistryById(String registryId, String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WORK_REGISTRY_JSON, issueId, registryId);
		JSONObject registry = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		WorkRegistryJsonBuilder builder = new WorkRegistryJsonBuilder();
		try{
			return builder.setJsonObj(registry).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IWorkRegistryDTO createIssueWorkRegistry(String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("workingDay", new SimpleDateFormat("dd/MM/yyyy").format(workingDay));
		queryParams.put("includeInSprint", Boolean.toString(includeInSprint));
		queryParams.put("description", description);
		queryParams.put("username", username);
		queryParams.put("time", Integer.toString(time));
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WORK_REGISTRY, issueId);
		String projectLocation = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(projectLocation, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueWorkRegistryById(retrievedId, issueId);
	}
	
	@Override
	public IWorkRegistryDTO updateIssueWorkRegistry(String registryId, String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("workingDay", new SimpleDateFormat("dd/MM/yyyy").format(workingDay));
		queryParams.put("includeInSprint", Boolean.toString(includeInSprint));
		queryParams.put("description", description);
		queryParams.put("username", username);
		queryParams.put("time", Integer.toString(time));
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WORK_REGISTRY, issueId) + "/" + registryId;
		String location = resourceDAO.update(broadcastService.getConnectionId(),
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(location, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueWorkRegistryById(retrievedId, issueId);
	}
	
	@Override
	public void deleteIssueWorkRegistry(String registryId, String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WORK_REGISTRY, issueId) + "/" + registryId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	/*---------------------------------*
     *      	Registres W.I.P.       *
     *---------------------------------*/
	@Override
	public List<IIssueWipRegistryDTO> findIssueWorkInProgressRegistriesByUser(String username) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_USER_WIP_REGISTRIES_JSON, username);
		JSONArray registries = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		
		List<IIssueWipRegistryDTO> resultList = new ArrayList<IIssueWipRegistryDTO>();
		IssueWipRegistryJsonBuilder builder = new IssueWipRegistryJsonBuilder();
		for(int i=0; i<registries.length(); i++){
			try{
				JSONObject registry = registries.getJSONObject(i);
				resultList.add(builder.setJsonObj(registry).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WIP_REGISTRY_SEARCH_JSON, issueId);
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("username", username);
		JSONObject registry = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		IssueWipRegistryJsonBuilder builder = new IssueWipRegistryJsonBuilder();
		try{
			return builder.setJsonObj(registry).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistryById(String registryId, String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WIP_REGISTRY_JSON, issueId, registryId);
		JSONObject registry = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		IssueWipRegistryJsonBuilder builder = new IssueWipRegistryJsonBuilder();
		try{
			return builder.setJsonObj(registry).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
		}
	}
	
	@Override
	public IIssueWipRegistryDTO createWorkInProgressRegistry(String issueId, String username) throws BusinessException
	{
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("username", username);
		
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WIP_REGISTRY, issueId);
		String location = resourceDAO.create(broadcastService.getConnectionId(), 
				credentialsProvider.getUsername(), 
				credentialsProvider.getPassword(), 
				url, 
				queryParams);
		String retrievedId = StringUtils.substringAfterLast(location, "/");
		retrievedId = StringUtils.substringBefore(retrievedId, ".json");
		return findIssueWorkInProgressRegistryById(retrievedId, issueId);
	}
	
	@Override
	public void deleteWorkInProgressRegistry(String registryId, String issueId) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WIP_REGISTRY, issueId) + "/" + registryId;
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
	}
	
	@Override
	public void deleteIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_ISSUE_WIP_REGISTRY, issueId);
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("username", username);
		resourceDAO.delete(broadcastService.getConnectionId(), credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
	}
}
