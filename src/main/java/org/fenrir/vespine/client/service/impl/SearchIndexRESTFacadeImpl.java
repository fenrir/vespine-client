package org.fenrir.vespine.client.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.fenrir.vespine.core.service.ISearchIndexFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140723
 */
public class SearchIndexRESTFacadeImpl implements ISearchIndexFacade 
{
	private static final String URL_INDEX = "/rest/index";
	
	private WebResource resource;
	
	@Inject
	private IUserCredentialsProvider credentialsProvider;

	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resource = Client.create().resource(endpoint);
	}
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	@Override
	public void createIndex() throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		ClientResponse response = resource.path(URL_INDEX)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(
						Base64.encode(credentialsProvider.getUsername() + ":" + credentialsProvider.getPassword())))
				.post(ClientResponse.class);
		if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			response.close();
			String cause = response.getEntity(String.class);
			throw new BusinessException("Error creant index: " + cause);
		}
		else if(response.getStatus()!=Response.Status.CREATED.getStatusCode()){
			response.close();
			throw new BusinessException("Error inespecífic creant index");	
		}
		
		response.close();
	}
	
	@Override
	public void enableIndex() throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("enable", Boolean.TRUE.toString());
		ClientResponse response = resource.path(URL_INDEX)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(
						Base64.encode(credentialsProvider.getUsername() + ":" + credentialsProvider.getPassword())))
				.put(ClientResponse.class);
		if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			response.close();
			String cause = response.getEntity(String.class);
			throw new BusinessException("Error habilitant index: " + cause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			response.close();
			throw new BusinessException("Error inespecífic habilitant index");	
		}
		
		response.close();
	}
	
	@Override
	public void disableIndex() throws BusinessException
	{
		if(resource==null){
			throw new IllegalStateException("No s'ha inicialitzat l'endpoint del servidor");
		}
		
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("enable", Boolean.FALSE.toString());
		ClientResponse response = resource.path(URL_INDEX)
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.header(HttpHeaders.AUTHORIZATION, "Basic " + new String(
						Base64.encode(credentialsProvider.getUsername() + ":" + credentialsProvider.getPassword())))
				.put(ClientResponse.class);
		if(response.getStatus()==Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()){
			response.close();
			String cause = response.getEntity(String.class);
			throw new BusinessException("Error deshabilitant index: " + cause);
		}
		else if(response.getStatus()!=Response.Status.OK.getStatusCode()){
			response.close();
			throw new BusinessException("Error inespecífic deshabilitant index");	
		}
		
		response.close();
	}
}
