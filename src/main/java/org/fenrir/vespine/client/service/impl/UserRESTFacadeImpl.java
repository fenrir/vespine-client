package org.fenrir.vespine.client.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.collections.map.MultiValueMap;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.builder.UserJsonBuilder;
import org.fenrir.vespine.client.dao.WebResourceJsonDAO;
import org.fenrir.vespine.client.service.RESTConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.vespine.core.service.IUserFacade;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140723
 */
public class UserRESTFacadeImpl implements IUserFacade 
{
	@Inject
	private IUserCredentialsProvider credentialsProvider;
	
	private WebResourceJsonDAO resourceDAO;
	
	public void setCredentialsProvider(IUserCredentialsProvider credentialsProvider)
	{
		this.credentialsProvider = credentialsProvider;
	}
	
	@Inject
	public void setEndpoint(@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)String endpoint)
	{
		resourceDAO = new WebResourceJsonDAO(endpoint);
	}
	
	@Override
	public List<IUserDTO> findAllActiveUsers() throws BusinessException
	{
		String url = RESTConstants.URL_USERS_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("active", Boolean.TRUE.toString());
		
		JSONArray users = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<IUserDTO> resultList = new ArrayList<IUserDTO>();
		UserJsonBuilder builder = new UserJsonBuilder();
		for(int i=0; i<users.length(); i++){
			try{
				JSONObject user = users.getJSONObject(i);
				resultList.add(builder.setJsonObj(user).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<IUserDTO> findUsersCompleteNameLike(String name) throws BusinessException
	{
		String url = RESTConstants.URL_USERS_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("completeNameLike", name);
		
		JSONArray users = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<IUserDTO> resultList = new ArrayList<IUserDTO>();
		UserJsonBuilder builder = new UserJsonBuilder();
		for(int i=0; i<users.length(); i++){
			try{
				JSONObject user = users.getJSONObject(i);
				resultList.add(builder.setJsonObj(user).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public List<IUserDTO> findUsersUsernameLike(String name) throws BusinessException
	{
		String url = RESTConstants.URL_USERS_JSON;
		MultiValueMap queryParams = new MultiValueMap();
		queryParams.put("usernameLike", name);
		
		JSONArray users = resourceDAO.getJsonValues(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url, queryParams);
		
		List<IUserDTO> resultList = new ArrayList<IUserDTO>();
		UserJsonBuilder builder = new UserJsonBuilder();
		for(int i=0; i<users.length(); i++){
			try{
				JSONObject user = users.getJSONObject(i);
				resultList.add(builder.setJsonObj(user).build());
			}
			catch(JSONException e){
				throw new BusinessException("Error interpretant la resposta del servidor: " + e.getMessage(), e);
			}
		}
		
		return resultList;
	}
	
	@Override
	public IUserDTO findUserByUsername(String username) throws BusinessException
	{
		String url = MessageFormat.format(RESTConstants.URL_USER_JSON, username);
		JSONObject user = resourceDAO.getSingleJsonValue(credentialsProvider.getUsername(), credentialsProvider.getPassword(), url);
		UserJsonBuilder builder = new UserJsonBuilder();
		try{
			return builder.setJsonObj(user).build();
		}
		catch(JSONException e){
			throw new BusinessException("Error interpretant resposta del servidor: " + e.getMessage(), e);
		}
	}
}
