package org.fenrir.vespine.client.util;

/**
 * TODO v1.0 Documentació
 * Credencials de proves
 * 		basicUser -> e549b54ad8f1f64bdb01240a29dcc1f9daf98c435489c46927910607fd10b34f
 * 		advancedUser -> b8e3d93afe1003189eeb1e0cf7bebabe71981a0bf53e8a1ddf16939adf5186d8
 * @author Antonio Archilla Nava
 * @version v0.1.20140620
 */
public interface IUserCredentialsProvider 
{
	public String getUsername();
	public String getPassword();
}
