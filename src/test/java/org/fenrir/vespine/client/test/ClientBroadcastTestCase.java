package org.fenrir.vespine.client.test;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;

import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.broadcast.service.IMessageProcessor;
import org.fenrir.vespine.client.broadcast.service.impl.ClientBroadcastServiceImpl;
import org.fenrir.vespine.client.test.util.CredentialsProviderTestImpl;
import org.fenrir.vespine.client.test.util.MessageProcessorTestImpl;
import org.fenrir.vespine.client.test.util.TestConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientBroadcastTestCase 
{
	private Injector injector;
	private CredentialsProviderTestImpl credentialsProvider;
	
	@Before
	public void initializeTestCase()
	{
		injector = Guice.createInjector(new AbstractModule() 
		{
			@Override
			protected void configure() 
			{
				bind(IClientBroadcastService.class).to(ClientBroadcastServiceImpl.class).in(Singleton.class);
				bind(IUserCredentialsProvider.class).to(CredentialsProviderTestImpl.class).in(Singleton.class);
				bind(IMessageProcessor.class).to(MessageProcessorTestImpl.class).in(Singleton.class);
				bind(String.class).annotatedWith(Names.named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)).toInstance(TestConstants.WS_ENDPOINT);
			}
		});
		
		credentialsProvider = (CredentialsProviderTestImpl)injector.getInstance(IUserCredentialsProvider.class);
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_OBSERVER_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_OBSERVER_PASSWORD);
	}
	
	@Test
	public void t1ConnectionTest() throws Exception
	{
		IClientBroadcastService clientService = injector.getInstance(IClientBroadcastService.class);
		clientService.connect();
		
		while(true){
			
		}
	}
}
