package org.fenrir.vespine.client.test;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.service.impl.AdministrationFacadeRESTImpl;
import org.fenrir.vespine.client.test.util.CredentialsProviderTestImpl;
import org.fenrir.vespine.client.test.util.TestConstants;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRESTTestCase 
{
	private static Injector injector;
	private static IAdministrationFacade administrationFacade;
	private static CredentialsProviderTestImpl credentialsProvider;
	
	@BeforeClass
	public static void initializeTestCase()
	{
		injector = Guice.createInjector(new AbstractModule() 
		{
			@Override
			protected void configure() 
			{
				bind(IAdministrationFacade.class).to(AdministrationFacadeRESTImpl.class).in(Singleton.class);
				bind(IUserCredentialsProvider.class).to(CredentialsProviderTestImpl.class).in(Singleton.class);
				bind(String.class).annotatedWith(Names.named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)).toInstance(TestConstants.WS_ENDPOINT);
			}
		});
		
		administrationFacade = injector.getInstance(IAdministrationFacade.class);
		credentialsProvider = (CredentialsProviderTestImpl)injector.getInstance(IUserCredentialsProvider.class);
	}
	
	@Test
	public void t1FindAllProjectsTest() throws BusinessException
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_OBSERVER_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_OBSERVER_PASSWORD);
		
		List<IProjectDTO> projects = administrationFacade.findAllProjects();
		Assert.assertTrue("ERROR: No s'han trobat projectes", projects.size()>0);
	}
	
	@Test
	public void t2findProjectsByWorkflowTest() throws BusinessException 
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_OBSERVER_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_OBSERVER_PASSWORD);
		
		List<IProjectDTO> projects = administrationFacade.findProjectsByWorkflow("1");
		Assert.assertTrue("ERROR: No s'han trobat projectes", projects.size()>0);
	}
	
	@Test
	public void t3findProjectsByIdTest() throws BusinessException 
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_OBSERVER_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_OBSERVER_PASSWORD);
		
		IProjectDTO project = administrationFacade.findProjectById("1");		
		Assert.assertNotNull("ERROR: No s'han trobat projectes", project);
	}
	
	@Test
	public void t4createProjectTest() throws BusinessException
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_ADMIN_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_ADMIN_PASSWORD);
		
		IProjectDTO project = administrationFacade.createProject("PROVES", "PR", "1", new ArrayList<String>());
		Assert.assertNotNull("ERROR: El projecte no s'ha creat correctament", project);
	}
	
	@Test
	public void t5updateProjectTest() throws BusinessException
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_ADMIN_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_ADMIN_PASSWORD);
		
		IProjectDTO project = administrationFacade.updateProject("1", "PROVES MOD", "1", new ArrayList<String>());
		Assert.assertNotNull("ERROR: El projecte no s'ha modificat correctament", project);
	}
	
	@Test
	public void t6deleteProjectTest() throws BusinessException
	{
		credentialsProvider.setUsername(TestConstants.CREDENTIALS_ADMIN_USERNAME);
		credentialsProvider.setPassword(TestConstants.CREDENTIALS_ADMIN_PASSWORD);
		
		administrationFacade.deleteProject("1");		
	}
}
