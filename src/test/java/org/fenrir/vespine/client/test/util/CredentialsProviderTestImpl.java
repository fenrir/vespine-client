package org.fenrir.vespine.client.test.util;

import org.fenrir.vespine.client.util.IUserCredentialsProvider;

public class CredentialsProviderTestImpl implements IUserCredentialsProvider 
{
	private String username;
	private String password;
	
	@Override
	public String getUsername() 
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}

	@Override
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
}
