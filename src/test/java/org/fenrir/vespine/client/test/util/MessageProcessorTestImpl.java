package org.fenrir.vespine.client.test.util;

import org.fenrir.vespine.client.broadcast.service.IMessageProcessor;
import org.fenrir.vespine.core.dto.BroadcastMessage;

public class MessageProcessorTestImpl implements IMessageProcessor 
{
	@Override
	public void processMessage(BroadcastMessage message) 
	{
		System.out.println("MESSAGE RECEIVED: " + message.toString());
	}
}
