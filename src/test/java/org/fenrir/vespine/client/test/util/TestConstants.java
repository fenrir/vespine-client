package org.fenrir.vespine.client.test.util;

public class TestConstants 
{
	public static final String WS_ENDPOINT = "http://localhost:8080/vespine";
	
	public static final String CREDENTIALS_ADMIN_USERNAME = "admin";
	public static final String CREDENTIALS_ADMIN_PASSWORD = "admin";
	public static final String CREDENTIALS_OBSERVER_USERNAME = "basicUser";
	public static final String CREDENTIALS_OBSERVER_PASSWORD = "basicUser";
	public static final String CREDENTIALS_REPORTER_USERNAME = "advancedUser";
	public static final String CREDENTIALS_REPORTER_PASSWORD = "advancedUser";
}
